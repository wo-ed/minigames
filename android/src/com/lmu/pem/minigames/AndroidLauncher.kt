package com.lmu.pem.minigames

import android.os.Build
import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_launcher.*

class AndroidLauncher : AndroidApplication() {
    private val osManager by lazy { AndroidOsManager(this, cameraView) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)

        FirebaseApp.initializeApp(this)

        val config = AndroidApplicationConfiguration()
        config.useCompass = true
        config.useWakelock = true

        val perms = arrayOf("android.permission.RECORD_AUDIO", "android.permission.CAMERA")
        val permsRequestCode = 200

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            requestPermissions(perms, permsRequestCode)
        }

        val gameView = initializeForView(MyGdxGame(osManager), config)
        placeholderView.addView(gameView)

        osManager.onActivityCreate()
    }

    override fun onStart() {
        super.onStart()
        osManager.onActivityStart()
    }

    override fun onResume() {
        super.onResume()
        osManager.onActivityResume()
    }

    override fun onPause() {
        super.onPause()
        osManager.onActivityPause()
    }

    override fun onStop() {
        super.onStop()
        osManager.onActivityStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        osManager.onActivityDestroy()
    }
}