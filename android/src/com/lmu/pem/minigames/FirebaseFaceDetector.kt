package com.lmu.pem.minigames

import android.app.Activity
import android.hardware.Camera
import android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK
import android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT
import android.util.Log
import android.util.SparseIntArray
import android.view.Surface
import android.view.View
import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import com.lmu.pem.minigames.osmanager.Face
import com.lmu.pem.minigames.osmanager.FaceDetector
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.preview.Frame
import io.fotoapparat.selector.back
import io.fotoapparat.selector.front
import io.fotoapparat.view.CameraView

class FirebaseFaceDetector(
        private val activity: Activity,
        private val cameraView: CameraView
) : FaceDetector {

    private val fotoapparat = Fotoapparat
            .with(activity)
            .into(cameraView)
            .previewScaleType(ScaleType.CenterCrop)
            .lensPosition(front())
            .build()

    private val cameraConfiguration = CameraConfiguration(
            frameProcessor = this::processFrame
    )

    private val cameraInfo = Camera.CameraInfo()
    private var cameraId = CAMERA_FACING_FRONT

    private var callback: ((Iterable<Face>) -> Unit)? = null

    private val options = FirebaseVisionFaceDetectorOptions.Builder()
            .setModeType(FirebaseVisionFaceDetectorOptions.FAST_MODE)
            .setLandmarkType(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
            .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
            .setMinFaceSize(0.1f)
            .setTrackingEnabled(false)
            .build()

    private val detector = FirebaseVision.getInstance().getVisionFaceDetector(options)

    private var isBusy = false

    private var isCameraEnabled = false
        set(value) {
            field = value
            if (value) {
                fotoapparat.start()
            } else {
                fotoapparat.stop()
            }
        }

    private fun processFrame(frame: Frame) {
        if (isBusy) {
            return
        }

        isBusy = true
        detectInFrame(frame).addOnSuccessListener { results ->
            val faces = results.map { it.toOsIndependentFace() }
            callback?.invoke(faces)
            isBusy = false
            Log.d(TAG, "$faces")
        }
    }

    private fun detectInFrame(frame: Frame): Task<List<FirebaseVisionFace>> {
        val rotation = getRotationCompensation()

        val metadata = FirebaseVisionImageMetadata.Builder()
                .setWidth(frame.size.width)
                .setHeight(frame.size.height)
                .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
                .setRotation(rotation)
                .build()

        val image = FirebaseVisionImage.fromByteArray(frame.image, metadata)
        return detectInImage(image)
    }

    private fun detectInImage(image: FirebaseVisionImage) = detector.detectInImage(image)

    override fun setUpCamera(front: Boolean) {
        val lens = if (front) front() else back()
        cameraId = if (front) CAMERA_FACING_FRONT else CAMERA_FACING_BACK
        fotoapparat.switchTo(lens, cameraConfiguration)
    }

    override fun startCamera() {
        isCameraEnabled = true
    }

    override fun stopCamera() {
        isCameraEnabled = false
    }

    override fun resumeCamera() {
        isCameraEnabled = true
    }

    override fun pauseCamera() {
        isCameraEnabled = false
    }

    override fun showCamera() {
        activity.runOnUiThread {
            cameraView.visibility = View.VISIBLE
        }
    }

    override fun hideCamera() {
        activity.runOnUiThread {
            cameraView.visibility = View.GONE
        }
    }

    override fun startFaceDetection(callback: (Iterable<Face>) -> Unit) {
        this.callback = callback
        isBusy = false
    }

    override fun stopFaceDetection() {
        callback = null
        isBusy = false
    }

    fun onActivityStart() {
        if (isCameraEnabled) {
            fotoapparat.start()
        }
    }

    fun onActivityStop() {
        fotoapparat.stop()
    }

    private fun getRotationCompensation(): Int {
        // Get the device's current rotation relative to its "native" orientation.
        // Then, from the ORIENTATIONS table, look up the angle the image must be
        // rotated to compensate for the device's rotation.
        val deviceRotation = activity.windowManager.defaultDisplay.rotation
        var rotationCompensation = ORIENTATIONS.get(deviceRotation)

        // On most devices, the sensor orientation is 90 degrees, but for some
        // devices it is 270 degrees. For devices with a sensor orientation of
        // 270, rotate the image an additional 180 ((270 + 270) % 360) degrees.
        /* val cameraManager = activity.getSystemService(CAMERA_SERVICE) as CameraManager
        val sensorOrientation = cameraManager
                .getCameraCharacteristics("1")
                .get(CameraCharacteristics.SENSOR_ORIENTATION)!! */
        Camera.getCameraInfo(cameraId, cameraInfo)
        val sensorOrientation = cameraInfo.orientation

        rotationCompensation = (rotationCompensation + sensorOrientation + 270) % 360

        // Return the corresponding FirebaseVisionImageMetadata rotation value.
        return when (rotationCompensation) {
            0 -> FirebaseVisionImageMetadata.ROTATION_0
            90 -> FirebaseVisionImageMetadata.ROTATION_90
            180 -> FirebaseVisionImageMetadata.ROTATION_180
            270 -> FirebaseVisionImageMetadata.ROTATION_270
            else -> {
                FirebaseVisionImageMetadata.ROTATION_0
            }
        }
    }

    companion object {
        private const val TAG = "FirebaseFaceDetector"

        private val ORIENTATIONS = SparseIntArray().apply {
            append(Surface.ROTATION_0, 90)
            append(Surface.ROTATION_90, 0)
            append(Surface.ROTATION_180, 270)
            append(Surface.ROTATION_270, 180)
        }
    }
}

fun FirebaseVisionFace.toOsIndependentFace() = Face(
        smilingProbability,
        leftEyeOpenProbability,
        rightEyeOpenProbability,
        headEulerAngleY,
        headEulerAngleZ
)