package com.lmu.pem.minigames

import android.app.Activity
import com.lmu.pem.minigames.osmanager.OsManager
import io.fotoapparat.view.CameraView

class AndroidOsManager(
        private val activity: Activity,
        private val cameraView: CameraView
) : OsManager {

    override lateinit var faceDetector: FirebaseFaceDetector
        private set

    override lateinit var headsetDetector: AndroidHeadsetDetector
        private set

    fun onActivityCreate() {
        faceDetector = FirebaseFaceDetector(activity, cameraView)
        headsetDetector = AndroidHeadsetDetector(activity)
    }

    fun onActivityStart() {
        faceDetector.onActivityStart()
    }

    fun onActivityResume() {

    }

    fun onActivityPause() {

    }

    fun onActivityStop() {
        faceDetector.onActivityStop()
    }

    fun onActivityDestroy() {

    }
}