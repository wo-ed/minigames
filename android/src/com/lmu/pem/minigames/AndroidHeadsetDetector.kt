package com.lmu.pem.minigames

import android.content.Context
import android.media.AudioManager
import com.lmu.pem.minigames.osmanager.HeadsetDetector

class AndroidHeadsetDetector(context: Context) : HeadsetDetector {
    private val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

    @Suppress("DEPRECATION")
    override fun isWiredHeadsetOn() = audioManager.isWiredHeadsetOn
}