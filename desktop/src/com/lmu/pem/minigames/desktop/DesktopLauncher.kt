package com.lmu.pem.minigames.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.lmu.pem.minigames.MyGdxGame

object DesktopLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration().apply {
            width = 432
            height = 768
        }
        val osManager = DesktopOsManager()
        val myGdxGame = MyGdxGame(osManager)
        LwjglApplication(myGdxGame, config)
    }
}