package com.lmu.pem.minigames.desktop

import com.lmu.pem.minigames.osmanager.FaceDetector
import com.lmu.pem.minigames.osmanager.HeadsetDetector
import com.lmu.pem.minigames.osmanager.OsManager

class DesktopOsManager : OsManager {
    override val faceDetector: FaceDetector? = null
    override val headsetDetector: HeadsetDetector? = null
}