package com.lmu.pem.minigames

import com.badlogic.gdx.Game
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.lmu.pem.minigames.ui.screens.BaseScreen

class BackKeyInputProcessor(private val game: Game) : InputAdapter() {

    override fun keyUp(keycode: Int): Boolean {

        when (keycode) {
            Input.Keys.BACK, Input.Keys.ESCAPE -> {
                val screen = game.screen as? BaseScreen
                screen?.let {
                    it.onBackPressed()
                    return true
                }
            }
        }

        return false
    }
}