package com.lmu.pem.minigames

import com.badlogic.gdx.maps.MapObject
import com.badlogic.gdx.maps.objects.*
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import ktx.box2d.body
import ktx.math.times

object MapBodyBuilder {

    private val temp = Vector2()

    fun buildBodies(objects: Iterable<MapObject>, world: World, scale: Float) =
            objects.mapNotNull { buildBody(it, world, scale) }

    fun buildBody(obj: MapObject, world: World, scale: Float): Body? {
        val shape = getShape(obj, scale) ?: return null

        val body = world.body {
            type = BodyType.StaticBody
            fixture(shape) { density = 1f }
        }

        shape.dispose()
        return body
    }

    fun getShape(obj: MapObject, scale: Float): Shape? {
        return when (obj) {
            is RectangleMapObject -> getRectangle(obj, scale)
            is PolygonMapObject -> getPolygon(obj, scale)
            is PolylineMapObject -> getPolyline(obj, scale)
            is CircleMapObject -> getCircle(obj, scale)
            else -> null
        }
    }

    fun getRectangle(rectangleObject: RectangleMapObject, scale: Float): PolygonShape {
        val rectangle = rectangleObject.rectangle
        val center = rectangle.getCenter(temp)

        return PolygonShape().apply {
            setAsBox(rectangle.width / 2f * scale, rectangle.height / 2f * scale, center * scale, 0.0f)
        }
    }

    fun getCircle(circleObject: CircleMapObject, scale: Float): CircleShape {
        val circle = circleObject.circle

        return CircleShape().apply {
            radius = circle.radius * scale
            position.x = circle.x * scale
            position.y = circle.y * scale
        }
    }

    fun getPolygon(polygonObject: PolygonMapObject, scale: Float): PolygonShape {
        val vertices = polygonObject.polygon.transformedVertices
        val worldVertices = vertices.map { it * scale }.toFloatArray()

        return PolygonShape().apply {
            set(worldVertices)
        }
    }

    fun getPolyline(polylineObject: PolylineMapObject, scale: Float): ChainShape {
        val vertices = polylineObject.polyline.transformedVertices
        val worldVertices = arrayOfNulls<Vector2>(vertices.size / 2)

        for (i in 0 until vertices.size / 2) {
            worldVertices[i] = Vector2().apply {
                x = vertices[i * 2] * scale
                y = vertices[i * 2 + 1] * scale
            }
        }

        return ChainShape().apply {
            createChain(worldVertices)
        }
    }
}
