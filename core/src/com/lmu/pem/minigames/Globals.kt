package com.lmu.pem.minigames

const val PADDING = 2f

const val MENU_VIEWPORT_WIDTH = 432f
const val MENU_VIEWPORT_HEIGHT = 768f
const val DEFAULT_GAME_VIEWPORT_WIDTH = 432f
const val DEFAULT_GAME_VIEWPORT_HEIGHT = 768f

const val DEFAULT_VIBRATION_TIME = 100

const val DEG_TO_RAD = (Math.PI / 180).toFloat()
const val RAD_TO_DEG = (180 / Math.PI).toFloat()