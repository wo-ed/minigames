package com.lmu.pem.minigames

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ExtendViewport

fun defaultGameStage() = Stage(
        ExtendViewport(
                DEFAULT_GAME_VIEWPORT_WIDTH,
                DEFAULT_GAME_VIEWPORT_HEIGHT
        ),
        context.inject()
)

fun halfHeightGameStage() = Stage(
        ExtendViewport(
                DEFAULT_GAME_VIEWPORT_WIDTH,
                DEFAULT_GAME_VIEWPORT_HEIGHT / 2f
        ),
        context.inject()
)

fun defaultUiStage() = Stage(
        ExtendViewport(
                MENU_VIEWPORT_WIDTH,
                MENU_VIEWPORT_HEIGHT
        ),
        context.inject()
)

fun halfHeightUiStage() = Stage(
        ExtendViewport(
                MENU_VIEWPORT_WIDTH,
                MENU_VIEWPORT_HEIGHT / 2f
        ),
        context.inject()
)