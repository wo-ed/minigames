package com.lmu.pem.minigames

class InfiniteSingleItemIterator<T>(private val t: T) : Iterator<T> {
    override fun hasNext() = true
    override fun next() = t
}