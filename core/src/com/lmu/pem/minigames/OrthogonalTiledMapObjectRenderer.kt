package com.lmu.pem.minigames

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.maps.MapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer

class OrthogonalTiledMapObjectRenderer(
        map: TiledMap,
        unitScale: Float,
        batch: Batch
) : OrthogonalTiledMapRenderer(map, unitScale, batch) {

    override fun renderObject(obj: MapObject) {
        val width = obj.properties["width"] as Float
        val height = obj.properties["height"] as Float

        if (obj is TiledMapTileMapObject) {
            batch.draw(
                    obj.textureRegion,
                    obj.x * unitScale,
                    obj.y * unitScale,
                    obj.originX * unitScale,
                    obj.originY * unitScale,
                    width * unitScale,
                    height * unitScale,
                    obj.scaleX * unitScale,
                    obj.scaleY * unitScale,
                    obj.rotation * unitScale
            )
        }
    }
}