package com.lmu.pem.minigames.games.fishattack

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import com.lmu.pem.minigames.ui.actors.TextureActor
import kotlin.math.abs

class FishAttackGameScreen : GameScreen() {
    init {
        this.isDrawBordersEnabled = true
    }
    override val gameStage = defaultGameStage()
    var listener: EventListener? = null
    override val assets = listOf(Assets.pufferfish, Assets.swordfish, Assets.seabg)
    override var bgColor: Color = Color.ROYAL
    @Volatile
    private var done: Boolean = false
    private var timeSeconds = 0f
    private var timer = 0L
    var time = 0L
    private val period = 0.2f
    private lateinit var pufferFish: Pufferfish
    private lateinit var swordFish: SwordFish

    override suspend fun initialize() {
        super.initialize()
        initializeMap()
        val pufferFishTexture: Texture = assetStorage[Assets.PUFFERFISH]!!
        val pufferFish = Pufferfish(pufferFishTexture, gameStage)

        val swordFishTexture: Texture = assetStorage[Assets.SWORDFISH]!!
        val swordFish = SwordFish(swordFishTexture, gameStage)

        gameStage.addActor(pufferFish)
        gameStage.addActor(swordFish)

        Thread(Runnable {
            val device = Gdx.audio.newAudioDevice(44100, true)
            val recorder = Gdx.audio.newAudioRecorder(44100, true)

            val pcmBuffer = ShortArray(1102)
            while (!done) {
                recorder.read(pcmBuffer, 0, pcmBuffer.size)
                timeSeconds += Gdx.graphics.rawDeltaTime

                System.out.println(timer)
                System.out.println(time)
                if (timeSeconds > period) {
                    var sum = 0
                    for (i in pcmBuffer.indices) {
                        sum += abs(pcmBuffer[i].toInt())
                    }
                    val average = sum / pcmBuffer.size
                    pufferFish.setIntensity(average)
                    timeSeconds -= period
                }
                if(pufferFish.isCollision(swordFish)) {
                    listener?.onFailure()
                    done = true
                }
            }
            device.dispose()
            recorder.dispose()
        }).start()
    }

    override fun render(delta: Float) {
        super.render(delta)
        if(!isPaused) {
            timer += delta.times(1000).toLong()
            if (timer >= time) {
                listener?.onSuccess()
                done = true
            }
        }

    }

    private fun initializeMap() {
        val seaBgTexture: Texture = assetStorage[Assets.SEABG]!!
        val backgroundActor = TextureActor(seaBgTexture)

        backgroundActor.width = gameStage.viewport.worldWidth
        backgroundActor.height = gameStage.viewport.worldWidth*706/969
        backgroundActor.y = 0f
        backgroundActor.x = gameStage.viewport.worldWidth/2 - backgroundActor.width/2

        gameStage.addActor(backgroundActor)
    }

    override fun dispose() {
        super.dispose()
        done = true
    }

    interface EventListener {
        fun onFailure()
        fun onSuccess()
    }
}