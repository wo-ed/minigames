package com.lmu.pem.minigames.games.fishattack

import com.lmu.pem.minigames.games.Minigame

class FishAttackGame: Minigame<FishAttackGameScreen>(10000L, FishAttackGameScreen(), hint = "Blow"),
        FishAttackGameScreen.EventListener{

    init {
        screen.listener = this
        screen.time = time
    }

    override fun onFailure() {
        isFailed = true
        System.out.println("Game Lost")
    }

    override fun onSuccess() {
        isFinished = true
    }
}