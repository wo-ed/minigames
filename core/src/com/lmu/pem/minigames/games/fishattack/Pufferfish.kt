package com.lmu.pem.minigames.games.fishattack

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.lmu.pem.minigames.ui.actors.TextureActor

class Pufferfish(texture: Texture?, gameStage: Stage?) : TextureActor() {
    private var currentIntensity = 0f
    private val intensityDecrease = 200
    private val minIntensity = 200
    private val intensityFactor = 50
    private var gameStage: Stage? = null

    private val size: Vector2 = Vector2(140f, 140f)

    private var collisionPoints: MutableList<Vector2> = mutableListOf()

    init {
        this.texture = texture
        this.gameStage = gameStage
        setSize(size.x, size.y)
        setPosition(30f, 500f)

    }

    fun setIntensity(intensity: Int) {
        if(intensity >= currentIntensity && intensity > minIntensity) {
            currentIntensity = intensity.toFloat()
        }
    }

    override fun act(delta: Float) {
        super.act(delta)
        moveFish(delta)
    }

    private fun moveFish(delta: Float) {
        y += this.currentIntensity / intensityFactor * delta

        for(p in collisionPoints) {
            p.y += this.currentIntensity / intensityFactor * delta
        }
        currentIntensity -= intensityDecrease
    }

    fun isCollision(swordFish: SwordFish): Boolean {
        if (arePointsColliding(collisionPoints, swordFish))  {
            System.out.println("C")
            return true
        }
        return false
    }

    private fun arePointsColliding(points: MutableList<Vector2>, s: SwordFish): Boolean {
            if (((x > s.x && x < s.x + s.width) && (y > s.y && y < s.y + s.height)) ||
                    (x > s.x && x < s.x + s.width) && (s.y > y && s.y < y + height)  ||
                    (s.x > x && s.x < x + width ) && (s.y > y && s.y < y + height) ||
                    (s.x > x && s.x < x + width ) && (y > s.y && y < s.y + s.height) ||
                    x < 0 || x > this.gameStage?.viewport!!.worldWidth || y < 0 || y > this.gameStage?.viewport!!.worldHeight) {
                var oX = s.x
                var oY = s.y
                System.out.println("Collided at o.x: $oX, o.y: $oY")
                return true
            }
        return false
    }

}