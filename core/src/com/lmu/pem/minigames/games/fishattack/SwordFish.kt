package com.lmu.pem.minigames.games.fishattack

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.Stage
import com.lmu.pem.minigames.ui.actors.TextureActor

class SwordFish(texture: Texture?, gameStage: Stage?) : TextureActor() {
    private val SPEED: Float = 200f
    private val gameStage: Stage? = gameStage
    init {
        this.texture = texture
        setPosition(gameStage?.viewport!!.worldWidth, gameStage?.viewport!!.worldHeight / 4)
        flipX = true
        this.setSize(250f, 75f)
    }

    override fun act(delta: Float) {
        super.act(delta)
        moveSwordFish(delta)
    }

    private fun moveSwordFish(delta: Float) {
        setPosition(x - SPEED * delta, y)
        if(x + width <= 0) {
            x = gameStage?.viewport!!.worldWidth
            y = (Math.random() * (gameStage?.viewport!!.worldHeight) + 1).toFloat()
        }
    }
}