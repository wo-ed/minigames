package com.lmu.pem.minigames.games.jump
import com.lmu.pem.minigames.games.Minigame

class JumpGame : Minigame<JumpGameScreen>(10000L, JumpGameScreen(), hint = "Shake!"),
        JumpGameScreen.EventListener {

    init {
        screen.listener = this
        screen.time = time
    }

    override fun onFailure() {
        isFailed = true
        System.out.println("Game Lost")
    }

    override fun onSuccess() {
        isFinished = true
    }
}