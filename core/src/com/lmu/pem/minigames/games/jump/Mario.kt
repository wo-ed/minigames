package com.lmu.pem.minigames.games.jump

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Shape2D
import com.lmu.pem.minigames.ui.actors.TextureActor




lateinit var marioRectangle: Rectangle

class Mario (texture: Texture?) : TextureActor(){



    init {
       // Rectangle bounds = new Rectangle(getX().toFloat(), getY().toFloat(), width.toFloat(), height.toFloat())
        this.texture = texture
       // setOrigin(width / 2, height / 2)
        setSize(200f,200f)
        //setPosition(Gdx.app.graphics.width.toFloat()/4 - this.width/2, Gdx.app.graphics.height.toFloat()/4-this.height/2)
        setPosition(50f, 50f)



    }

    override fun act(delta: Float) {
        super.act(delta)
        marioRectangle = Rectangle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat()-100, this.height.toInt().toFloat())
    }

    fun getBounds()
    : Rectangle {
        return marioRectangle
    }

}






