package com.lmu.pem.minigames.games.jump

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction
import com.lmu.pem.minigames.ui.actors.TextureActor


class Goomba (texture: Texture?) : TextureActor() {

    lateinit var goombaRectangle: Rectangle

    init {
        this.texture = texture
       // setOrigin(width / 2, height / 2)
        setSize(100f, 100f)
        //setPosition(Gdx.app.graphics.width.toFloat()/4 - this.width/2, Gdx.app.graphics.height.toFloat()/4-this.height/2)
        setPosition(400f, 50f)
    }

    override fun act(delta: Float) {

        super.act(delta)
        if(this.actions.size == 0){
            goombaWalk()
        }
        goombaRectangle = Rectangle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat()-100, this.height.toInt().toFloat())
    }

    private fun goombaWalk(){

        val newposition = MathUtils.random(400f,700f)
        val durationRand = MathUtils.random(3f, 5f)
        var ac1 = MoveToAction()
        ac1.setPosition(-200f, 50f)
        ac1.setDuration(4f)

        this.addAction(ac1)

        this.setPosition(newposition,50f)

        this.addAction(ac1)



    }

    fun getBounds()
            : Rectangle {
        return goombaRectangle
    }

}
