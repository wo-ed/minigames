package com.lmu.pem.minigames.games.jump

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.ContactListener
import com.badlogic.gdx.physics.box2d.Manifold
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.NumberUtils;
import com.badlogic.gdx.utils.Scaling;
import kotlin.concurrent.timer


class JumpGameScreen : GameScreen(){

    //lateint var batch : SpriteBatch
    override val gameStage = defaultGameStage()

    private var goombas: MutableList<Goomba> = mutableListOf()

    var listener: EventListener? = null


    override val assets = listOf(Assets.mario, Assets.goomba, Assets.jumpSound, Assets.arghSound, Assets.shell, Assets.boy)
    override var bgColor: Color = Color.SKY

    private lateinit var mario: Mario
    private lateinit var goomba1: Goomba

    lateinit var jumpSound : Sound
    lateinit var arghSound : Sound
    var time = 0L
    private var timer = 0L

    //private lateinit var rectMario : Rectangle

    override suspend fun initialize() {
        //var timer : Float
        super.initialize()
        val marioTexture: Texture = assetStorage[Assets.MARIO]!!
        val goombaTexture: Texture = assetStorage[Assets.GOOMBA]!!
        val shellTexture: Texture = assetStorage[Assets.SHELL]!!
        val boyTexture: Texture = assetStorage[Assets.BOY]!!
        jumpSound = assetStorage[Assets.JUMP]!!
        arghSound = assetStorage[Assets.ARGH]!!

        //Create Mario
        mario = Mario(boyTexture)
        gameStage.addActor(mario)

        //Create Goomba1
        goomba1 = Goomba(shellTexture)
        gameStage.addActor(goomba1)

        goombas.add(goomba1)

    }

    override fun render(delta: Float) {

        super.render(delta)

        if(!isPaused) {
            timer += delta.times(1000).toLong()
            println(timer / 1000f)
        }

        var accelX = Gdx.input.accelerometerX
        var accelY = Gdx.input.accelerometerY
        var accelZ = Gdx.input.accelerometerZ

        if(timer > time){
            listener?.onSuccess()
        }

        if(mario.getBounds().overlaps(goomba1.getBounds())){
            println("DEAD")
            arghSound.play()
            listener?.onFailure()
        }
        if(mario.actions.size == 0) {
            if (accelY > 19.8 || accelX > 19.8 || accelZ > 19.8) {

                marioJump()
                jumpSound.play()
            }
        }


    }

    private fun marioJump() {
        var ac1 = MoveToAction()
        ac1.setPosition(50f, 400f)
        ac1.setDuration(0.5f)

        var ac2 = MoveToAction()
        ac2.setPosition(50f, 50f)
        ac2.setDuration(0.5f)

        var sequence = SequenceAction()
        sequence.addAction(ac1)
        sequence.addAction(ac2)

        mario.addAction(sequence)
    }




    interface EventListener {
        fun onFailure()
        fun onSuccess()
    }
}