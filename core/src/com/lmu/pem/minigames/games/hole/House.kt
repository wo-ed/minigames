package com.lmu.pem.minigames.games.hole

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.lmu.pem.minigames.ui.actors.TextureActor


class House (texture: Texture?, xPos:Float, yPos:Float) : TextureActor() {

    lateinit var houseRectangle: Rectangle
    lateinit var basketballCircle : Circle

    var accelRand = 0f

    init {
        this.texture = texture
        this.x = xPos
        this.y = yPos

        setSize(100f,100f)

        // pop = Gdx.audio.newSound(Gdx.files.internal("sounds/baloon_pop.ogg"))
    }

    override fun act(delta: Float) {
        super.act(delta)
        houseRectangle = Rectangle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat()-50, this.height.toInt().toFloat()-50)

    }

    fun getBounds()
            : Rectangle {
        return houseRectangle
    }
    }