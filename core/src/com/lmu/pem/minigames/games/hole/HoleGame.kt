package com.lmu.pem.minigames.games.hole
import com.lmu.pem.minigames.games.Minigame
import com.lmu.pem.minigames.games.dummy.HoleGameScreen

class HoleGame : Minigame<HoleGameScreen>(15000L, HoleGameScreen(), hint = "Tilt"),
        HoleGameScreen.EventListener {
    init {
        screen.listener = this
    }

    override fun onFailure() {
        isFailed = true
        System.out.println("Game Lost")
    }

    override fun onSuccess() {
        isFinished = true
    }
}