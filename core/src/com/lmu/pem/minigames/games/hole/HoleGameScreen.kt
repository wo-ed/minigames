package com.lmu.pem.minigames.games.dummy

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import com.lmu.pem.minigames.games.hole.Basketball
import com.lmu.pem.minigames.games.hole.House

class HoleGameScreen : GameScreen() {
    override val gameStage = defaultGameStage()
    var listener: HoleGameScreen.EventListener? = null
    override var bgColor: Color = Color.BLACK

    private lateinit var red_ball: Basketball
    private lateinit var blue_ball: Basketball
    private lateinit var green_ball: Basketball
    private lateinit var yellow_ball: Basketball

    private lateinit var red_house: House
    private lateinit var blue_house: House
    private lateinit var green_house: House
    private lateinit var yellow_house: House

    lateinit var ball_in : Sound

    private var blue_removed = false
    private var red_removed = false
    private var yellow_removed = false
    private var green_removed = false

    lateinit var arghSound : Sound



    var winCount = 0


    // These assets are loaded when the screen is initialized.
    override val assets = listOf(Assets.basketball_texture, Assets.red_circle, Assets.red_house, Assets.green_house,
            Assets.yellow_house, Assets.blue_house, Assets.arghSound, Assets.yellow_ball, Assets.red_ball, Assets.green_ball,
            Assets.blue_ball
    )



    override suspend fun initialize() {
        super.initialize()

        val skin: Skin = context.inject()
        val labelStyle = skin.get("light", Label.LabelStyle::class.java)
        setLabelStyle(labelStyle)

        val basketball_texture: Texture = assetStorage[Assets.BBALL]!!
        val green_house_texture: Texture = assetStorage[Assets.GREEN_HOUSE]!!
        val red_house_texture: Texture = assetStorage[Assets.RED_HOUSE]!!
        val blue_house_texture: Texture = assetStorage[Assets.BLUE_HOUSE]!!
        val yellow_house_texture: Texture = assetStorage[Assets.YELLOW_HOUSE]!!

        val red_ball_texture: Texture = assetStorage[Assets.RED_BALL]!!
        val green_ball_texture: Texture = assetStorage[Assets.GREEN_BALL]!!
        val blue_ball_texture: Texture = assetStorage[Assets.BLUE_BALL]!!
        val yellow_ball_texture: Texture = assetStorage[Assets.YELLOW_BALL]!!

        ball_in = Gdx.audio.newSound(Gdx.files.internal("sounds/ball_in.wav"))

        arghSound = assetStorage[Assets.ARGH]!!

        //initialize houses, not random
        green_house = House(green_house_texture, 25f, 25f)  //unten links
        red_house = House(red_house_texture, 25f, 600f)     //oben links
        blue_house = House(blue_house_texture, 325f, 25f)   //unten rechts
        yellow_house = House(yellow_house_texture, 325f, 600f)  //oben rechts

        //initialize basketballs
        red_ball = Basketball(red_ball_texture)
        green_ball = Basketball(green_ball_texture)
        blue_ball = Basketball(blue_ball_texture)
        yellow_ball = Basketball(yellow_ball_texture)

        //spawn houses  first (so balls are in front)
        gameStage.addActor(blue_house)
        gameStage.addActor(red_house)
        gameStage.addActor(yellow_house)
        gameStage.addActor(green_house)

        //spawn basketballs
        gameStage.addActor(red_ball)
        gameStage.addActor(green_ball)
        gameStage.addActor(yellow_ball)
        gameStage.addActor(blue_ball)
    }

    override fun updateGame(delta: Float) {
        super.updateGame(delta)

        if(winCount == 4)
        {
            listener?.onSuccess()
        }

        //red ball
        if(red_house.getBounds().overlaps(red_ball.getCircleBounds()) && red_removed==false){
            red_ball.removeBasketball()
            ball_in.play()
            red_removed = true
            winCount++

        }
        //blue ball
        if(blue_house.getBounds().overlaps(blue_ball.getCircleBounds())&& blue_removed==false){
            blue_ball.removeBasketball()
            ball_in.play()
            blue_removed = true
            winCount++
        }
        //green ball
        if(green_house.getBounds().overlaps(green_ball.getCircleBounds())&& green_removed==false){
            green_ball.removeBasketball()
            ball_in.play()
            green_removed = true
            winCount++
        }
        //yellow ball
        if(yellow_house.getBounds().overlaps(yellow_ball.getCircleBounds())&& yellow_removed==false){
            yellow_ball.removeBasketball()
            ball_in.play()
            yellow_removed = true
            winCount++
        }

        }




    interface EventListener {
        fun onFailure()
        fun onSuccess()
    }
}