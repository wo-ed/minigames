package com.lmu.pem.minigames.games.hole

import com.badlogic.gdx.graphics.Texture
import com.lmu.pem.minigames.ui.actors.TextureActor
import java.util.Random
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.balloons.Crosshair
import com.lmu.pem.minigames.osmanager.HeadsetDetector
import com.lmu.pem.minigames.osmanager.OsManager
import java.util.concurrent.ThreadLocalRandom


class Basketball (texture: Texture?) : TextureActor() {


    private var speedX = MathUtils.random(0.5f, 2f)
    private var speedY = MathUtils.random(0.5f, 2f)

    lateinit var basketballRectangle: Rectangle
    lateinit var basketballCircle : Circle
    private var radius = 0f

    var accelRand = 0f

    init {
        this.texture = texture
        x = MathUtils.random(50f, 550f)
        y = MathUtils.random(50f, 550f)


        radius = width


        if(this.x<250){
            accelRand = -1f
        }
        if(this.x>250) {
            accelRand = 1f
        }
        setSize(50f,50f)

        // pop = Gdx.audio.newSound(Gdx.files.internal("sounds/baloon_pop.ogg"))
    }



    override fun act(delta: Float) {
        super.act(delta)
        basketballRectangle = Rectangle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat()-50, this.height.toInt().toFloat()-50)
        basketballCircle = Circle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat())
        moveBasketball()

    }



    fun removeBasketball(){
        this.remove()
    }

    fun getRectBounds()
            : Circle {
        return basketballCircle
    }

    fun getCircleBounds()
            : Rectangle {
        return basketballRectangle
    }

    fun changeSpeed(newSpeedx: Float, newSpeedy : Float){
        speedX = newSpeedx
        speedY = newSpeedy
    }

    fun moveBasketball() {

        val accelX = Gdx.input.accelerometerX
        val accelY = Gdx.input.accelerometerY
        //linker screenrand
        if ((this.x < 0)) {
            this.x = 0f
        }
        //unterer screenrand
        if ((this.y < 0)) {
            this.y = 0f
        }

        //rechter screenrand
        if (this.x + width > stage.width)
         {
            x = stage.width - width
         }
        //oberer screenrand
        if (this.y + height > stage.height)
         {
            y = stage.height - height
         }
        else{
            this.x += accelX * 0.8f * speedX * accelRand
            this.y += accelY * 0.8f * speedY * accelRand
        }
    }


}