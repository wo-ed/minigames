package com.lmu.pem.minigames.games.sail

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.lmu.pem.minigames.ui.actors.TextureActor

class Boat(texture: Texture?, gameStage: Stage?) : TextureActor() {
    private var currentIntensity = 0f
    private val intensityDecrease = 100
    private val minIntensity = 200
    private val intensityFactor = 100
    private val rotationFactor = 5
    private var gameStage: Stage? = null

    private val size: Vector2 = Vector2(70f, 70f)

    private var frontPoint: Vector2 = Vector2(0f, 0f)
    private var sideTopPointL: Vector2 = Vector2(0f, 0f)
    private var sideTopPointR: Vector2 = Vector2(0f, 0f)
    private var sideMidPointL: Vector2 = Vector2(0f, 0f)
    private var sideMidPointR: Vector2 = Vector2(0f, 0f)
    private var bottomPoint: Vector2 = Vector2(0f, 0f)

    private var collisionPoints: MutableList<Vector2> = mutableListOf()

    init {
        this.texture = texture
        this.gameStage = gameStage
        setSize(size.x, size.y)
        setOrigin(width/2, height/2)
        setPosition(170f, 0f)

        //init points for collision
        frontPoint.set(x + size.x/2, y + height)
        sideTopPointL.set(x + size.x/4 + 20f, y + height - 20)
        sideTopPointR.set(x + size.x/4 * 3 - 20f, y + height - 20)
        sideMidPointL.set(x + 50f, height/2)
        sideMidPointR.set(x + width - 50f, height/2)
        bottomPoint.set(x + width/2, y)

        collisionPoints.add(frontPoint)
        collisionPoints.add(sideTopPointL)
        collisionPoints.add(sideTopPointR)
        collisionPoints.add(sideMidPointL)
        collisionPoints.add(sideMidPointR)
        collisionPoints.add(bottomPoint)

        for(p in collisionPoints) {
            var xP = p.x
            var yP = p.y
            System.out.println("x: $xP, y: $yP")
        }
    }

    fun setIntensity(intensity: Int) {
        if(intensity >= currentIntensity && intensity > minIntensity) {
            currentIntensity = intensity.toFloat()
        }
    }

    override fun act(delta: Float) {
        super.act(delta)
        rotateBoat(delta)
        moveBoat(delta)
    }

    private fun moveBoat(delta: Float) {
        y += this.currentIntensity / intensityFactor * MathUtils.cosDeg(rotation) * delta
        x += this.currentIntensity / intensityFactor * (-MathUtils.sinDeg(rotation)) * delta

        for(p in collisionPoints) {
            p.y += this.currentIntensity / intensityFactor * MathUtils.cosDeg(rotation) * delta
            p.x += this.currentIntensity / intensityFactor * (-MathUtils.sinDeg(rotation)) * delta
        }

        if(currentIntensity > intensityDecrease) {
            currentIntensity -= intensityDecrease
        }
    }

    private fun rotateBoat(delta: Float) {
        val roll = Gdx.input.roll
        rotation -= roll * rotationFactor * delta

        for(p in collisionPoints) {

        }
    }

    fun isCollision(obstacles: MutableList<Obstacle>): Boolean {
        for (o in obstacles) {
            if (arePointsColliding(collisionPoints, o))  {
                System.out.println("C")
                return true
            }

        }
        return false
    }

    fun isGameWon(g: Obstacle): Boolean {
        if (arePointsColliding(collisionPoints, g))  {
            System.out.println("C")
            return true
        }
        return false
    }

    private fun arePointsColliding(points: MutableList<Vector2>, o: Obstacle): Boolean {
        for(p in points) {
            if (((p.x > o.x && p.x < o.x + o.width) && (p.y > o.y && p.y < o.y + o.height)) ||
                    p.x < 0 || p.x > this.gameStage?.viewport!!.worldWidth || p.y < 0 || p.y > this.gameStage?.viewport!!.worldHeight) {
                var oX = o.x
                var oY = o.y
                System.out.println("Collided at o.x: $oX, o.y: $oY")
                return true
            }
        }
        return false
    }

}