package com.lmu.pem.minigames.games.sail

import com.badlogic.gdx.graphics.Texture
import com.lmu.pem.minigames.ui.actors.TextureActor

class Obstacle(texture: Texture?, xPos: Float, yPos: Float, width: Float, height: Float) : TextureActor() {
    init {
        this.texture = texture
        setOrigin(xPos - width/2, yPos - height/2)
        setPosition(xPos, yPos)
        this.setSize(width, height)
    }
}