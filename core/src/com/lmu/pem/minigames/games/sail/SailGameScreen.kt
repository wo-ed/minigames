package com.lmu.pem.minigames.games.sail

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import kotlin.math.abs

class SailGameScreen : GameScreen() {
    init {
        this.isDrawBordersEnabled = true
    }
    override val gameStage = defaultGameStage()
    var listener: EventListener? = null
    override val assets = listOf(Assets.island, Assets.pirateIsland, Assets.harbourWall, Assets.boat, Assets.stones, Assets.goal, Assets.lighthouse, Assets.oceanSailTrack)
    override var bgColor: Color = Color.ROYAL
    @Volatile
    private var done: Boolean = false
    private var timeSeconds = 0f
    private val period = 0.2f
    private var obstacles: MutableList<Obstacle> = mutableListOf()
    private lateinit var goal: Obstacle


    override suspend fun initialize() {
        super.initialize()
        initializeMap()
        val boatTexture: Texture = assetStorage[Assets.BOAT]!!
        val boat = Boat(boatTexture, gameStage)

        val oceanSound: Music = assetStorage[Assets.SEA_TRACK]!!
        oceanSound.isLooping = true
        oceanSound.volume = 0.2f
        oceanSound.play()
        gameStage.addActor(boat)

        Thread(Runnable {
            val device = Gdx.audio.newAudioDevice(44100, true)
            val recorder = Gdx.audio.newAudioRecorder(44100, true)

            val pcmBuffer = ShortArray(1102)
            while (!done) {
                recorder.read(pcmBuffer, 0, pcmBuffer.size)
                timeSeconds += Gdx.graphics.rawDeltaTime
                if (timeSeconds > period) {
                    var sum = 0
                    for (i in pcmBuffer.indices) {
                        sum += abs(pcmBuffer[i].toInt())
                    }
                    val average = sum / pcmBuffer.size
                    boat.setIntensity(average)
                    timeSeconds -= period
                }
                if(boat.isCollision(obstacles)) {
                    listener?.onFailure()
                    done = true
                }else if(boat.isGameWon(goal)) {
                    listener?.onSuccess()
                    done = true
                }
            }
            device.dispose()
            recorder.dispose()
        }).start()
    }

    private fun initializeMap() {
        
        val islandTexture: Texture = assetStorage[Assets.ISLAND]!!
        val islandPirateTexture: Texture = assetStorage[Assets.ISLAND_PIRATE]!!
        val stones: Texture = assetStorage[Assets.STONES]!!
        val harbourWall: Texture = assetStorage[Assets.HARBOUR_WALL]!!
        val goalTexture: Texture = assetStorage[Assets.GOAL]!!
        val lighthouse: Texture = assetStorage[Assets.LIGHTHOUSE]!!

        goal = Obstacle(goalTexture, 50f, gameStage.viewport.worldHeight - 140f, 84f, 140f)
        obstacles.add(Obstacle(islandTexture, 230f, 160f, 150f, 150f))
        obstacles.add(Obstacle(islandPirateTexture, 5f, 170f, 170f, 170f))
        obstacles.add(Obstacle(stones, 150f, 430f, 200f, 200f))
        obstacles.add(Obstacle(lighthouse, 300f, 700f, 100f, 100f))

        gameStage.addActor(goal)
        for(o in obstacles) {
            gameStage.addActor(o)
        }
    }

    override fun dispose() {
        super.dispose()
        done = true
    }

    interface EventListener {
        fun onFailure()
        fun onSuccess()
    }
}