package com.lmu.pem.minigames.games.sail

import com.lmu.pem.minigames.games.Minigame

class SailGame: Minigame<SailGameScreen>(20000L, SailGameScreen(), hint = "Tilt and blow"),
        SailGameScreen.EventListener{

    init {
        screen.listener = this
    }

    override fun onFailure() {
        isFailed = true
        System.out.println("Game Lost")
    }

    override fun onSuccess() {
        isFinished = true
    }
}