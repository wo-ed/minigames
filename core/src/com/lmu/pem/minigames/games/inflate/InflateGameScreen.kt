package com.lmu.pem.minigames.games.inflate

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import kotlin.math.abs

class InflateGameScreen : GameScreen() {
    init {
        this.isDrawBordersEnabled = true
    }
    override val gameStage = defaultGameStage()
    var listener: EventListener? = null
    override val assets = listOf(Assets.red_balloon)
    @Volatile
    private var done: Boolean = false
    private var timeSeconds = 0f
    private val period = 0.2f

    override suspend fun initialize() {
        super.initialize()

        val balloonTexture: Texture = assetStorage[Assets.RED_BALLOON]!!
        val balloon = Balloon(balloonTexture, gameStage)

        gameStage.addActor(balloon)

        Thread(Runnable {
            val device = Gdx.audio.newAudioDevice(44100, true)
            val recorder = Gdx.audio.newAudioRecorder(44100, true)

            val pcmBuffer = ShortArray(1102)
            while (!done) {
                recorder.read(pcmBuffer, 0, pcmBuffer.size)
                timeSeconds += Gdx.graphics.rawDeltaTime
                if (timeSeconds > period) {
                    var sum = 0
                    for (i in pcmBuffer.indices) {
                        sum += abs(pcmBuffer[i].toInt())
                    }
                    val average = sum / pcmBuffer.size
                    balloon.setIntensity(average)
                    timeSeconds -= period
                }
                if(balloon.isBigEnough()) {
                    listener?.onSuccess()
                    done = true
                }
            }
            device.dispose()
            recorder.dispose()
        }).start()
    }

    override fun dispose() {
        super.dispose()
        done = true
    }

    interface EventListener {
        fun onFailure()
        fun onSuccess()
    }
}