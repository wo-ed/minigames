package com.lmu.pem.minigames.games.inflate

import com.lmu.pem.minigames.games.Minigame

class InflateGame: Minigame<InflateGameScreen>(5000L, InflateGameScreen(), hint = "Blow"),
        InflateGameScreen.EventListener{

    init {
        screen.listener = this
    }

    override fun onFailure() {
        isFailed = true
        System.out.println("Game Lost")
    }

    override fun onSuccess() {
        isFinished = true
    }
}