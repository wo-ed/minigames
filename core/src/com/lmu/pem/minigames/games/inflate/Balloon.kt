package com.lmu.pem.minigames.games.inflate

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.lmu.pem.minigames.ui.actors.TextureActor

class Balloon (texture: Texture?, gameStage: Stage?) : TextureActor() {
    private val minSize = 1.0f
    private val size: Vector2 = Vector2(100f, 100f)
    private var scaleFactor = 1.0f
    private var currentIntensity = 0f

    private val intensityDecrease = 0.1f
    private val intensityFactor = 5000
    private val minIntensity = 200

    init {
        this.texture = texture
        this.setSize(size.x, size.y)
        this.setPosition(gameStage?.viewport!!.worldWidth / 2 - width / 2, 0f)
        this.setOrigin(width / 2, 0f)
    }

    override fun act(delta: Float) {
        super.act(delta)
        inflate(delta)
    }

    fun setIntensity(intensity: Int) {
        if(intensity > minIntensity) {
            currentIntensity = intensity.toFloat() / intensityFactor
        }
    }

    fun inflate(delta: Float) {
        scaleFactor += currentIntensity * delta
        scaleFactor += currentIntensity * delta
        setScale(scaleFactor, scaleFactor)

        if(scaleFactor > minSize) {
            currentIntensity -= intensityDecrease
        }

        if(scaleFactor < minSize) {
            scaleFactor = 1.0f;
        }
        System.out.println("x: $scaleFactor")
    }

    fun isBigEnough(): Boolean {
        if(scaleFactor >= 7) {
            return true
        }
        return false
    }
}