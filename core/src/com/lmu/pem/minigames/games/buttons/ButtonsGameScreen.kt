package com.lmu.pem.minigames.games.buttons

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.assets.onClickWithSound
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import ktx.scene2d.textButton

class ButtonsGameScreen : GameScreen() {
    override val gameStage = defaultGameStage()

    var listener: InteractionListener? = null

    override val assets: Iterable<AssetDescriptor<*>>? = null

    override suspend fun initialize() {
        super.initialize()
        initButtons()
    }

    private fun initButtons() {
        gameControlsTable.apply {
            textButton("Do not click") {
                onClickWithSound { listener?.onWrongClick() }
                color = Color.GREEN
            }
            row()
            textButton("Click") {
                onClickWithSound { listener?.onRightClick() }
                color = Color.RED
            }
        }
    }

    interface InteractionListener {
        fun onRightClick()
        fun onWrongClick()
    }
}