package com.lmu.pem.minigames.games.buttons

import com.lmu.pem.minigames.games.Minigame

class ButtonsGame : Minigame<ButtonsGameScreen>(5000L, ButtonsGameScreen()),
        ButtonsGameScreen.InteractionListener {

    init {
        screen.listener = this
    }

    override fun onRightClick() {
        isFinished = true
    }

    override fun onWrongClick() {
        isFailed = true
    }
}