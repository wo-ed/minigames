package com.lmu.pem.minigames.games

import com.badlogic.gdx.utils.Disposable
import com.lmu.pem.minigames.MyGdxGame
import com.lmu.pem.minigames.context

open class Minigame<S : GameScreen>(
        val time: Long,
        val screen: S,
        val hint: String? = null
) : Disposable {

    private val myGdxGame: MyGdxGame = context.inject()

    open var isFinished = false
    open var isFailed = false

    private var isInitializing = false
    var isInitialized = false
        private set

    protected var isPaused = false
        set(value) {
            field = value
            screen.isPaused = value
        }

    suspend fun preload() {
        if (!isInitializing && !isInitialized) {
            isInitializing = true
            initialize()
            isInitialized = true
        }
    }

    protected open suspend fun initialize() {
        screen.loadAssets()
        screen.initialize()
    }

    open fun onStartGame() {}

    open fun onStopGame() {}

    open fun onPauseGame() {
        isPaused = true
    }

    open fun onResumeGame() {
        isPaused = false
    }

    override fun dispose() {
        screen.dispose()
        if (screen === myGdxGame.screen) {
            myGdxGame.screen = null
        }
        onStopGame()
    }
}