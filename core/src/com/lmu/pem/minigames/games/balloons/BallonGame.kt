package com.lmu.pem.minigames.games.balloons
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.games.Minigame
import com.lmu.pem.minigames.osmanager.OsManager

class BallonGame : Minigame<BalloonGameScreen>(8000L, BalloonGameScreen(), "Touch"),
        BalloonGameScreen.EventListener {

    private val headsetDetector = context.inject<OsManager>().headsetDetector

    init {
        screen.listener = this
        screen.headsetDetector = headsetDetector
    }

    override fun onFailure() {
        isFailed = true
        System.out.println("Game Lost")
    }

    override fun onSuccess() {
        isFinished = true
    }
}