package com.lmu.pem.minigames.games.balloons

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import com.badlogic.gdx.graphics.Texture
import com.lmu.pem.minigames.osmanager.HeadsetDetector

class BalloonGameScreen : GameScreen() {
    override val gameStage = defaultGameStage()

    var listener: EventListener? = null
    var headsetDetector: HeadsetDetector? = null

    override var bgColor: Color = Color.SKY

    private lateinit var crosshair: Crosshair
    private lateinit var balloon1: Balloon
    private lateinit var balloon2: Balloon
    private lateinit var balloon3: Balloon
    private lateinit var balloon4: Balloon

    private var unplugged: Boolean = true
    private var removed1 : Boolean = false
    private var removed2 : Boolean = false
    private var removed3 : Boolean = false
    private var removed4 : Boolean = false

    private var BalloonsDead = 0

    //var balloonPop = Gdx.audio.newSound(Gdx.files.internal("sounds/baloon_pop.ogg"))
    lateinit var balloonPop : Sound

    // These assets are loaded when the screen is initialized.
    override val assets = listOf(Assets.green_balloon, Assets.red_balloon, Assets.cross
            ,Assets.balloonPop
    )



    override suspend fun initialize() {
        super.initialize()
        val texture_green: Texture = assetStorage[Assets.GREEN_BALLOON]!!
        val texture_red: Texture = assetStorage[Assets.RED_BALLOON]!!
        val texture_cross: Texture = assetStorage[Assets.CROSSHAIR]!!
        balloonPop = assetStorage[Assets.POP]!!
        //val balloonPop: Sound = assetStorage[Assets.POP]!!

        //initialize Balloons&Crosshair
        balloon1 = Balloon(texture_red)
        balloon2 = Balloon(texture_green)
        balloon3 = Balloon(texture_red)
        balloon4 = Balloon(texture_green)
        crosshair = Crosshair(texture_cross)

        //add Balloons
        gameStage.addActor(balloon1)
        gameStage.addActor(balloon2)
        gameStage.addActor(balloon3)
        gameStage.addActor(balloon4)

        //add Crosshair
        gameStage.addActor(crosshair)

        //sailTrack.setLooping(1000, true)
       // sailTrack.play()

    }

    override fun updateGame(delta: Float) {
        super.updateGame(delta)
        //TouchEvent Detection

        if(Gdx.input.justTouched()){

            if (crosshair.getBounds().overlaps(balloon1.getBounds()) && removed1==false) {
                println("Balloon1 remove")
                balloonPop.play()
                balloon1.removeBalloon()
                //balloon2.changeSpeed(4f,4f)
                removed1=true

                BalloonsDead = BalloonsDead + 1
            }


            if (crosshair.getBounds().overlaps(balloon2.getBounds()) && removed2==false) {
                println("Balloon2 remove")
                balloon2.removeBalloon()
                //balloon1.changeSpeed(4f,4f)
                removed2=true
                balloonPop.play()
                BalloonsDead = BalloonsDead + 1
            }

            if (crosshair.getBounds().overlaps(balloon3.getBounds()) && removed3==false) {
                println("Balloon3 remove")
                balloon3.removeBalloon()
                //balloon2.changeSpeed(4f,4f)
                removed3=true
                balloonPop.play()
                BalloonsDead = BalloonsDead + 1
            }


            if (crosshair.getBounds().overlaps(balloon4.getBounds()) && removed4==false) {
                println("Balloon4 remove")
                balloon4.removeBalloon()
                //balloon1.changeSpeed(4f,4f)
                removed4=true
                balloonPop.play()
                BalloonsDead = BalloonsDead + 1
            }

        }

        if(BalloonsDead == 4){
            gameStage.clear()
            listener?.onSuccess()
        }

        // optional Headphone detection

        if (unplugged == true) {
           // println("unplugged = true")
            if (headsetDetector?.isWiredHeadsetOn() == true) {
                print("headset True")
                if (crosshair.getBounds().overlaps(balloon1.getBounds()) && removed1==false) {
                    println("Balloon1 remove")
                    balloon1.removeBalloon()
                    balloon2.changeSpeed(4f,4f)                               //new Speed
                    unplugged=false                                                                 //force user to unplug headphones
                    removed1=true                                                                   //check if already removed
                    balloonPop.play()
                    BalloonsDead = BalloonsDead + 1                                                 //Balloon Count
                }

                if (crosshair.getBounds().overlaps(balloon2.getBounds()) && removed2==false) {
                    println("Balloon2 remove")
                    balloon2.removeBalloon()
                    balloon1.changeSpeed(4f,4f)
                    unplugged=false
                    removed2=true
                    balloonPop.play()
                    BalloonsDead = BalloonsDead + 1
                }

                if (crosshair.getBounds().overlaps(balloon3.getBounds()) && removed3==false) {
                    println("Balloon3 remove")
                    balloon3.removeBalloon()
                    balloon4.changeSpeed(4f,4f)
                    unplugged=false
                    removed3=true
                    balloonPop.play()
                    BalloonsDead = BalloonsDead + 1
                }


                if (crosshair.getBounds().overlaps(balloon4.getBounds()) && removed4==false) {
                    println("Balloon4 remove")
                    balloon4.removeBalloon()
                    balloon3.changeSpeed(4f,4f)
                    unplugged=false
                    removed4=true
                    balloonPop.play()
                    BalloonsDead = BalloonsDead + 1
                }
            }
        }
        if (unplugged == false) {
            //println("unplugged = false")
            if (headsetDetector?.isWiredHeadsetOn() == false) {
                unplugged = true;
            }
        }


    }

    interface EventListener {
        fun onFailure()
        fun onSuccess()
    }
}