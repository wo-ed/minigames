package com.lmu.pem.minigames.games.balloons

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Rectangle
import com.lmu.pem.minigames.ui.actors.TextureActor

class Crosshair (texture:Texture): TextureActor(){

    var crosshairRectangle: Rectangle
    private var currentroll = 0f

    init {
        this.texture = texture
        //setOrigin(width/2, height/2)
        setPosition(180f, 350f)
        this.setSize(100f,100f)
        crosshairRectangle = Rectangle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat()-60, this.height.toInt().toFloat()-60)

        //println(crosshairRectangle)
    }

    override fun act(delta: Float) {
        crosshairRectangle = Rectangle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat()-60, this.height.toInt().toFloat()-60)
        super.act(delta)                        //Update every frame
        moveCrosshair()
    }

    fun getBounds()
            : Rectangle {
        return crosshairRectangle
    }

    fun moveCrosshair(){

        val roll = Gdx.input.roll
        //println(roll)
       // println("this.x: "+this.x)
        //linker screen
        if((this.x  < 0)) {
           // this.x += -roll * 0.4f
            this.x = 0f
          //  println("we are in 1st if")
            //this.x=this.x
            //return
        }

        //rechter screenrand
        else if((this.x + width> stage.width)
        ) {
            x = stage.width - width
            //println("we are in 2nd if")
            //this.x=this.x
            //this.x += -roll * 0.4f
            //return
        }
        else {
           // println("we are in else")
            this.x += roll * 0.4f
            currentroll= roll
        }
    }

}