package com.lmu.pem.minigames.games.balloons

import com.badlogic.gdx.graphics.Texture
import com.lmu.pem.minigames.ui.actors.TextureActor
import java.util.Random
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.balloons.Crosshair
import com.lmu.pem.minigames.osmanager.HeadsetDetector
import com.lmu.pem.minigames.osmanager.OsManager


class Balloon (texture: Texture?) : TextureActor() {


    private var speedX = MathUtils.random(2f, 4f)
    private var speedY = MathUtils.random(2f, 4f)
    private val headsetDetector = context.inject<OsManager>().headsetDetector

    lateinit var balloonRectangle: Rectangle

    lateinit var pop : Sound


    init {
        this.texture = texture
        //x = Gdx.graphics.width.toFloat()/2;
        //y = Gdx.graphics.height.toFloat()/2;    //Start @ Display Center
        x = MathUtils.random(50f, 200f)
        y = MathUtils.random(50f, 200f)
       // setOrigin(width /2, height/2);
        setSize(100f,100f)
       // pop = Gdx.audio.newSound(Gdx.files.internal("sounds/baloon_pop.ogg"))
    }



    override fun act(delta: Float) {
        super.act(delta)
        bounceBalloon()//Update every frame
        //moveBalloon(delta)
        balloonRectangle = Rectangle(this.x.toInt().toFloat(), this.y.toInt().toFloat(), this.width.toInt().toFloat()-50, this.height.toInt().toFloat()-50)
       // println("positionx : " + x)
        //println("postiony : " + y)
        //println(balloonRectangle)


    }

    fun bounceBalloon(){
        //println("HEADSET: ${headsetDetector?.isWiredHeadsetOn()}")

        //x-Achse
        if (x > stage.width - width || x  < 0) {
            //Here we flip the speed, so it bonces the other way.
            speedX = -speedX
        }
        //y-Achse
        if (y > stage.height - height || y < 0) {
            speedY = -speedY
        }

        x += speedX
        y += speedY
    }

    fun removeBalloon(){
        //this.remove()
        //addAction(Actions.removeActor())
        this.setPosition(500f,500f)
        this.speedY = 0f
        this.speedX = 0f
    }

    fun getBounds()
            : Rectangle {
        return balloonRectangle
    }

    fun changeSpeed(newSpeedx: Float, newSpeedy : Float){
        speedX = newSpeedx
        speedY = newSpeedy
    }

}