package com.lmu.pem.minigames.games.rock

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.lmu.pem.minigames.ui.actors.TextureActor


    class DiscoBall (texture: Texture?) : TextureActor() {

        init {
            this.texture = texture
            x = MathUtils.random(50f, 550f)
            y = MathUtils.random(50f, 550f)
            setSize(50f,50f)
        }



        override fun act(delta: Float) {
            super.act(delta)
        }

}