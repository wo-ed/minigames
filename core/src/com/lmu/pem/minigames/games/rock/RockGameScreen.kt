package com.lmu.pem.minigames.games.rock

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.assets.get
import com.lmu.pem.minigames.defaultGameStage
import com.lmu.pem.minigames.games.GameScreen
import com.lmu.pem.minigames.ui.actors.TextureActor
import com.badlogic.gdx.graphics.g2d.*


class RockGameScreen : GameScreen() {
    override val gameStage = defaultGameStage()
    var listener: RockGameScreen.EventListener? = null
    override var bgColor: Color = Color.BLUE

    private lateinit var disco_ball: DiscoBall
    private lateinit var rock_music: Music

    lateinit var Rockactor: TextureActor
    lateinit var MusicActor : TextureActor
    lateinit var playerAnimation: Animation<Texture>

    //lateinit var batch : SpriteBatch
    //lateinit var font : BitmapFont



    var elapsedTime = 0f




    // These assets are loaded when the screen is initialized.
    override val assets = listOf(Assets.disco_ball, Assets.music, Assets.rockMusic).union(Assets.rocker)



    override suspend fun initialize() {
        super.initialize()

        val rockerTextures = assetStorage.get(Assets.rocker).filterNotNull()
        playerAnimation = Animation(0.1f, *rockerTextures.toTypedArray())

        val sound_texture: Texture = assetStorage[Assets.MUSIC]!!

        //load guitar sound
        rock_music = assetStorage[Assets.ROCK_MUSIC]!!

        //initialize actor (rocker)
        Rockactor = TextureActor(playerAnimation)
        Rockactor.setPosition(120f,180f)
        Rockactor.setSize(250f,250f)
        Rockactor.animation?.playMode = Animation.PlayMode.LOOP

        //initialize music notes (actor)
        MusicActor = TextureActor(sound_texture)
        MusicActor.setPosition(270f, 250f)
        MusicActor.setSize(50f, 50f)

        gameStage.addActor(Rockactor)
        gameStage.addActor(MusicActor)

        //set volume low, looping
        rock_music.volume = 0.01f
        rock_music.isLooping = true
        rock_music.play()








    }

    override fun updateGame(delta: Float) {

        super.updateGame(delta)



        val accelX = Gdx.input.accelerometerX
        val accelY = Gdx.input.accelerometerY
        val accelZ = Gdx.input.accelerometerZ

        println(rock_music.volume)

        if(MusicActor.actions.size == 0){
            moveMusicActor()
        }

        //win condition
        if(rock_music.volume>1.7f)
        {
            listener?.onSuccess()
            rock_music.dispose()
        }

        //louder volume condition
        if(accelX>19.8 || accelY > 19.8 || accelZ>19.8){
            println("we are higher!!!")
            rock_music.setVolume(rock_music.volume + 0.05f)
        }


    }

    private fun moveMusicActor()
    {
        var action1 = MoveToAction()
        action1.setPosition(270f, 600f)
        action1.duration = 2f

        MusicActor.addAction(action1)

        MusicActor.setPosition(270f,250f)

        MusicActor.addAction(action1)
    }



    interface EventListener {
        fun onFailure()
        fun onSuccess()
    }
}