package com.lmu.pem.minigames.games.rock

import com.lmu.pem.minigames.games.Minigame

class RockGame : Minigame<RockGameScreen>(5000L, RockGameScreen(), hint = "Shake!"),
        RockGameScreen.EventListener {
    init {
        screen.listener = this
    }

    override fun onFailure() {
        isFailed = true
        System.out.println("Game Lost")
    }

    override fun onSuccess() {
        isFinished = true
    }
}