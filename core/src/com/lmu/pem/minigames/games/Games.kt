package com.lmu.pem.minigames.games

import com.lmu.pem.minigames.games.balloons.BallonGame
import com.lmu.pem.minigames.games.buttons.ButtonsGame
import com.lmu.pem.minigames.games.camera.anyonehome.AnyoneHomeGame
import com.lmu.pem.minigames.games.camera.behindyou.BehindYouGame
import com.lmu.pem.minigames.games.camera.blind.BlindGame
import com.lmu.pem.minigames.games.camera.hide.HideGame
import com.lmu.pem.minigames.games.camera.racing.RacingGame
import com.lmu.pem.minigames.games.camera.smile.SmileGame
import com.lmu.pem.minigames.games.fishattack.FishAttackGame
import com.lmu.pem.minigames.games.hole.HoleGame
import com.lmu.pem.minigames.games.inflate.InflateGame
import com.lmu.pem.minigames.games.jump.JumpGame
import com.lmu.pem.minigames.games.rock.RockGame
import com.lmu.pem.minigames.games.sail.SailGame

val games = linkedMapOf(
        JumpGame::class to GameCreator("Jump") { JumpGame() },
        SmileGame::class to GameCreator("Why so serious?") { SmileGame() },
        InflateGame::class to GameCreator("Inflate") { InflateGame() },
        BlindGame::class to GameCreator("Blinded by the light") { BlindGame() },
        HoleGame::class to GameCreator("Balls") { HoleGame() },
        BehindYouGame::class to GameCreator("Behind you") { BehindYouGame() },
        RockGame::class to GameCreator("Rock it!") { RockGame() },
        FishAttackGame::class to GameCreator("Dodge the Swordfish") { FishAttackGame() },
        HideGame::class to GameCreator("Hide") { HideGame() },
        SailGame::class to GameCreator("Sail") { SailGame() },
        AnyoneHomeGame::class to GameCreator("Anyone home?") { AnyoneHomeGame() },
        ButtonsGame::class to GameCreator("Buttons") { ButtonsGame() },
        BallonGame::class to GameCreator("Balloon") { BallonGame() },
        RacingGame::class to GameCreator("Racing") { RacingGame() }
)

class GameCreator(val title: String, val create: () -> Minigame<*>)
