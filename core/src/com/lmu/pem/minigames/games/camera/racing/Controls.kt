package com.lmu.pem.minigames.games.camera.racing

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.lmu.pem.minigames.osmanager.Face

interface Controls {
    // Must return either -1, 0 or 1
    val controlState: Int
}

class ArrowKeysControls(private val horizontal: Boolean) : Controls {
    override val controlState: Int
        get() {
            val minusKey = if (horizontal) Input.Keys.LEFT else Input.Keys.DOWN
            val plusKey = if (horizontal) Input.Keys.RIGHT else Input.Keys.UP
            val isMinusPressed = Gdx.input.isKeyPressed(minusKey)
            val isPlusPressed = Gdx.input.isKeyPressed(plusKey)

            return when {
                isMinusPressed && !isPlusPressed -> -1
                isPlusPressed && !isMinusPressed -> 1
                else -> 0
            }
        }
}

class TouchControls(private val horizontal: Boolean) : Controls {
    override val controlState: Int
        get() {
            val point = if (horizontal) Gdx.input.x else -Gdx.input.y
            val size = if (horizontal) Gdx.graphics.width else -Gdx.graphics.height

            return when {
                !Gdx.input.isTouched -> 0
                point < size / 2f -> -1
                point > size / 2f -> 1
                else -> 0
            }
        }
}

class HeadControls(val f: () -> Face?) : Controls {
    override val controlState: Int
        get() {
            val face = f()
            return when {
                face == null -> 0
                face.headEulerAngleZ <= -HEAD_ANGLE_THRESHOLD -> -1
                face.headEulerAngleZ >= HEAD_ANGLE_THRESHOLD -> 1
                else -> 0
            }
        }

    companion object {
        private const val HEAD_ANGLE_THRESHOLD = 4f
    }
}

class EyeControls(private val f: () -> Face?) : Controls {
    override val controlState: Int
        get() {
            val face = f()

            return when {
                face == null -> 0
                // face.isLeftEyeClosed && !face.isRightEyeClosed-> -1
                // face.isRightEyeClosed && !face.isLeftEyeClosed -> 1
                face.leftEyeOpenProbability <= 0.3 && face.rightEyeOpenProbability >= 0.5 -> -1
                face.leftEyeOpenProbability >= 0.5 && face.rightEyeOpenProbability <= 0.3 -> 1
                else -> 0
            }
        }
}

class PitchControls : Controls {
    override val controlState: Int
        get() {
            val pitch = Gdx.input.pitch
            return when {
                pitch < -360 / 8f -> -1
                pitch >= -360 / 8f -> 1
                else -> 0
            }
        }
}

class RollControls : Controls {
    override val controlState: Int
        get() {
            val roll = Gdx.input.roll
            return when {
                roll <= -360 / 16f -> -1
                roll >= 360 / 16f -> 1
                else -> 0
            }
        }
}