package com.lmu.pem.minigames.games.camera

import com.badlogic.gdx.Gdx
import com.lmu.pem.minigames.DEFAULT_VIBRATION_TIME
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.games.GameScreen
import com.lmu.pem.minigames.games.Minigame
import com.lmu.pem.minigames.osmanager.Face
import com.lmu.pem.minigames.osmanager.OsManager

abstract class FaceDetectionGame<S : GameScreen>(
        time: Long,
        screen: S,
        usesFrontCamera: Boolean = true,
        hint: String? = null
) : Minigame<S>(time, screen, hint) {

    private val faceDetector = context.inject<OsManager>().faceDetector

    override var isFinished: Boolean
        get() = super.isFinished
        set(value) {
            if (value && !super.isFinished) {
                Gdx.input.vibrate(DEFAULT_VIBRATION_TIME)
            }
            super.isFinished = value
        }

    protected var usesFrontCamera = usesFrontCamera
        set(value) {
            field = value
            faceDetector?.setUpCamera(value)
        }

    protected abstract fun onProcessFrame(faces: Iterable<Face>)

    override suspend fun initialize() {
        faceDetector?.setUpCamera(usesFrontCamera)
        faceDetector?.startCamera()
        super.initialize()
    }

    override fun onStartGame() {
        super.onStartGame()
        faceDetector?.showCamera()
        faceDetector?.startFaceDetection(::onProcessFrame)
    }

    override fun onStopGame() {
        super.onStopGame()
        faceDetector?.hideCamera()
        faceDetector?.stopFaceDetection()
        faceDetector?.stopCamera()
    }

    override fun onResumeGame() {
        super.onResumeGame()
        faceDetector?.resumeCamera()
    }

    override fun onPauseGame() {
        super.onPauseGame()
        faceDetector?.pauseCamera()
    }
}