package com.lmu.pem.minigames.games.camera.racing

import com.lmu.pem.minigames.games.camera.FaceDetectionGame
import com.lmu.pem.minigames.osmanager.Face

class RacingGame : FaceDetectionGame<RacingGameScreen>(
        30000L,
        RacingGameScreen(),
        usesFrontCamera = true,
        hint = "Close one eye\nTouch left or right"
), RacingGameScreen.InteractionListener {

    override fun isGameFinished() {
        isFinished = true
    }

    init {
        screen.listener = this
    }

    override fun onProcessFrame(faces: Iterable<Face>) {
        screen.face = faces.firstOrNull()
    }
}