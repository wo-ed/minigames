package com.lmu.pem.minigames.games.camera.hide

import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.games.camera.HalfHeightImageGameScreen
import ktx.actors.txt

class HideGameScreen : HalfHeightImageGameScreen(textureAsset = null) {
    fun setStyle(isHideEnabled: Boolean) {
        if (isHideEnabled) {
            messageLabel.txt = "Hide"
            setTextColor(Color.WHITE)
            bgColor = Color.BLACK
        } else {
            messageLabel.txt = "Show yourself"
            setTextColor(Color.BLACK)
            bgColor = Color.WHITE
        }
    }
}