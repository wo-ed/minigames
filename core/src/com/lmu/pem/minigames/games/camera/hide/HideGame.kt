package com.lmu.pem.minigames.games.camera.hide

import com.badlogic.gdx.Gdx
import com.lmu.pem.minigames.DEFAULT_VIBRATION_TIME
import com.lmu.pem.minigames.games.camera.FaceDetectionGame
import com.lmu.pem.minigames.osmanager.Face

class HideGame : FaceDetectionGame<HideGameScreen>(5000L, HideGameScreen()) {

    private var isHideEnabled: Boolean = false
        set(value) {
            field = value
            screen.setStyle(value)
        }

    init {
        isHideEnabled = false
    }

    override fun onProcessFrame(faces: Iterable<Face>) {
        if (!isPaused) {
            if (!isHideEnabled && faces.any()) {
                Gdx.input.vibrate(DEFAULT_VIBRATION_TIME)
                isHideEnabled = true
            } else if (isHideEnabled && !faces.any()) {
                isFinished = true
            }
        }
    }
}