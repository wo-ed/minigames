package com.lmu.pem.minigames.games.camera.behindyou

import com.lmu.pem.minigames.games.camera.FaceDetectionGame
import com.lmu.pem.minigames.osmanager.Face

class BehindYouGame : FaceDetectionGame<BehindYouGameScreen>(10000L, BehindYouGameScreen()) {

    override fun onProcessFrame(faces: Iterable<Face>) {
        if (!isPaused && faces.count() >= 2) {
            isFinished = true
        }
    }
}