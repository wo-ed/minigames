package com.lmu.pem.minigames.games.camera.smile

import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.games.camera.HalfHeightImageGameScreen

class SmileGameScreen : HalfHeightImageGameScreen(
        Assets.stare,
        "Why so serious?",
        textColor = Color.RED,
        bgColor = Color.BLACK
)