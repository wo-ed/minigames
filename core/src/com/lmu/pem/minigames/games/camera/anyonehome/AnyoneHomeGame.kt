package com.lmu.pem.minigames.games.camera.anyonehome

import com.lmu.pem.minigames.games.camera.FaceDetectionGame
import com.lmu.pem.minigames.osmanager.Face

class AnyoneHomeGame : FaceDetectionGame<AnyoneHomeGameScreen>(
        7000L,
        AnyoneHomeGameScreen(),
        usesFrontCamera = false
) {
    override fun onProcessFrame(faces: Iterable<Face>) {
        if (!isPaused && faces.any()) {
            isFinished = true
        }
    }
}