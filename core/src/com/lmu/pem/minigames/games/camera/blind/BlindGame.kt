package com.lmu.pem.minigames.games.camera.blind

import com.lmu.pem.minigames.games.camera.FaceDetectionGame
import com.lmu.pem.minigames.osmanager.Face

class BlindGame : FaceDetectionGame<BlindGameScreen>(5000L, BlindGameScreen()) {

    override fun onProcessFrame(faces: Iterable<Face>) {
        if (!isPaused && faces.any {
                    it.leftEyeOpenProbability <= 0.3 && it.rightEyeOpenProbability <= 0.3
                }) {
            isFinished = true
        }
    }
}