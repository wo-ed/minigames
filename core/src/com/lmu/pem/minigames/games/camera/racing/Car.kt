package com.lmu.pem.minigames.games.camera.racing

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
import com.lmu.pem.minigames.DEG_TO_RAD
import com.lmu.pem.minigames.RAD_TO_DEG
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.context
import ktx.async.assets.AssetStorage
import ktx.box2d.body

class Car(world: World, x: Float, y: Float, angle: Float) {
    private val assetStorage: AssetStorage = context.inject()

    val body = world.body {
        type = BodyDef.BodyType.DynamicBody
        position.set(x, y)
        this.angle = angle
        polygon(
                Vector2(1.5f, 0f),
                Vector2(3f, 2.5f),
                Vector2(2.8f, 5.5f),
                Vector2(1f, 10f),
                Vector2(-1f, 10f),
                Vector2(-2.8f, 5.5f),
                Vector2(-3f, 2.5f),
                Vector2(-1.5f, 0f)
        ) { density = 0.1f }
    }

    lateinit var flJoint: RevoluteJoint
    lateinit var frJoint: RevoluteJoint
    val tires: List<Tire>

    val sprite by lazy {
        val texture: Texture = assetStorage[Assets.CAR]!!
        Sprite(texture).apply {
            setSize(7f, 13f)
            setOrigin(width / 2f, height / 2f)
        }
    }

    init {
        val jointDef = RevoluteJointDef().apply {
            bodyA = body
            enableLimit = true
            lowerAngle = 0f // with both these at zero...
            upperAngle = 0f // ... the joint will not move
            localAnchorB.setZero() // joint anchor in tire is always center
        }

        val backLeftTire = Tire(world, x, y, angle).apply {
            setCharacteristics(MAX_FORWARD_SPEED, MAX_BACKWARD_SPEED, BACK_TIRE_MAX_DRIVE_FORCE, BACK_TIRE_MAX_LATERAL_IMPULSE)
            jointDef.bodyB = body
            jointDef.localAnchorA.set(-3f, 0.75f)
            world.createJoint(jointDef)
        }

        val backRightTire = Tire(world, x, y, angle).apply {
            setCharacteristics(MAX_FORWARD_SPEED, MAX_BACKWARD_SPEED, BACK_TIRE_MAX_DRIVE_FORCE, BACK_TIRE_MAX_LATERAL_IMPULSE)
            jointDef.bodyB = body
            jointDef.localAnchorA.set(3f, 0.75f)
            world.createJoint(jointDef)
        }

        val frontLeftTire = Tire(world, x, y, angle).apply {
            setCharacteristics(MAX_FORWARD_SPEED, MAX_BACKWARD_SPEED, FRONT_TIRE_MAX_DRIVE_FORCE, FRONT_TIRE_MAX_LATERAL_IMPULSE)
            jointDef.bodyB = body
            jointDef.localAnchorA.set(-3f, 8.5f)
            flJoint = world.createJoint(jointDef) as RevoluteJoint
        }

        val frontRightTire = Tire(world, x, y, angle).apply {
            setCharacteristics(MAX_FORWARD_SPEED, MAX_BACKWARD_SPEED, FRONT_TIRE_MAX_DRIVE_FORCE, FRONT_TIRE_MAX_LATERAL_IMPULSE)
            jointDef.bodyB = body
            jointDef.localAnchorA.set(3f, 8.5f)
            frJoint = world.createJoint(jointDef) as RevoluteJoint
        }

        tires = listOf(backLeftTire, backRightTire, frontLeftTire, frontRightTire)
    }

    fun updateControlState(controlState: Int) {
        tires.forEach { it.updateFriction() }
        tires.forEach { it.updateDrive(controlState) }
        tires.forEach { it.updateTurn(controlState) }

        // control steering
        val lockAngle = 35f * DEG_TO_RAD
        val turnSpeedPerSec = 160f * DEG_TO_RAD
        val turnPerTimeStep = turnSpeedPerSec / 60.0f
        val desiredAngle = when (controlState and (TDC_LEFT or TDC_RIGHT)) {
            TDC_LEFT -> lockAngle
            TDC_RIGHT -> -lockAngle
            else -> 0f
        }

        val angleNow = flJoint.jointAngle
        var angleToTurn = desiredAngle - angleNow
        angleToTurn = MathUtils.clamp(angleToTurn, -turnPerTimeStep, turnPerTimeStep)
        val newAngle = angleNow + angleToTurn
        flJoint.setLimits(newAngle, newAngle)
        frJoint.setLimits(newAngle, newAngle)
    }

    fun synchronizeSprite() {
        sprite.x = body.worldCenter.x - sprite.width / 2f
        sprite.y = body.worldCenter.y - sprite.height / 2f
        sprite.rotation = body.angle * RAD_TO_DEG
    }

    fun draw(batch: Batch) {
        sprite.draw(batch)
    }

    companion object {
        const val MAX_FORWARD_SPEED = 250f
        const val MAX_BACKWARD_SPEED = -40f
        const val BACK_TIRE_MAX_DRIVE_FORCE = 300f
        const val FRONT_TIRE_MAX_DRIVE_FORCE = 500f
        const val BACK_TIRE_MAX_LATERAL_IMPULSE = 14f
        const val FRONT_TIRE_MAX_LATERAL_IMPULSE = 7f
    }
}