package com.lmu.pem.minigames.games.camera.anyonehome

import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.games.camera.HalfHeightImageGameScreen

class AnyoneHomeGameScreen : HalfHeightImageGameScreen(
        textureAsset = null,
        text = "Anyone home?",
        textColor = Color.BLACK,
        bgColor = Color.WHITE
)