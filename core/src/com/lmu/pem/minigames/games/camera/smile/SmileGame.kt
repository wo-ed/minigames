package com.lmu.pem.minigames.games.camera.smile

import com.lmu.pem.minigames.games.camera.FaceDetectionGame
import com.lmu.pem.minigames.osmanager.Face

class SmileGame : FaceDetectionGame<SmileGameScreen>(5000L, SmileGameScreen()) {

    override fun onProcessFrame(faces: Iterable<Face>) {
        if (!isPaused && faces.any { it.isSmiling }) {
            isFinished = true
        }
    }
}