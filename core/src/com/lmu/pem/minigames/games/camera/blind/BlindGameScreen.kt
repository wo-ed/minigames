package com.lmu.pem.minigames.games.camera.blind

import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.games.camera.HalfHeightImageGameScreen

class BlindGameScreen : HalfHeightImageGameScreen(
        Assets.tunnel,
        "Blinded by the light",
        textColor = Color.WHITE,
        bgColor = Color.BLACK
)