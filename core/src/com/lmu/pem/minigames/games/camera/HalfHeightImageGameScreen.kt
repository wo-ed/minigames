package com.lmu.pem.minigames.games.camera

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Align
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.games.GameScreen
import com.lmu.pem.minigames.halfHeightGameStage
import com.lmu.pem.minigames.halfHeightUiStage
import com.lmu.pem.minigames.ui.actors.TextureActor
import ktx.actors.txt

open class HalfHeightImageGameScreen(
        protected val textureAsset: AssetDescriptor<Texture>? = null,
        protected val text: String = "",
        protected val textColor: Color? = null,
        bgColor: Color? = null
) : GameScreen() {

    override val gameStage = halfHeightGameStage()
    override val uiStage = halfHeightUiStage()
    override val assets: Iterable<AssetDescriptor<*>>? = textureAsset?.let { listOf(it) }

    protected val actor by lazy {
        val texture: Texture? = textureAsset?.let { assetStorage[it.fileName]!! }
        TextureActor(texture)
    }

    val messageLabel = Label("", context.inject<Skin>())

    init {
        bgColor?.let { this.bgColor = bgColor }
        messageLabel.txt = text
    }

    override suspend fun initialize() {
        super.initialize()
        initLayout()
    }

    open fun initLayout() {
        textColor?.let { setTextColor(it) }

        messageLabel.setAlignment(Align.center, Align.center)

        gameControlsTable.apply {
            add(messageLabel)
            row()
            add(actor)
        }
    }

    protected fun setTextColor(color: Color) {
        val skin: Skin = context.inject()
        val labelStyle = skin.get("light", Label.LabelStyle::class.java)
        setLabelStyle(labelStyle)
        messageLabel.color = color
        timeLabel.color = color
    }

    override fun setLabelStyle(style: Label.LabelStyle) {
        super.setLabelStyle(style)
        messageLabel.style = style
    }
}