package com.lmu.pem.minigames.games.camera.behindyou

import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.games.camera.HalfHeightImageGameScreen

class BehindYouGameScreen : HalfHeightImageGameScreen(
        textureAsset = null,
        text = "There is someone\nbehind you.",
        textColor = Color.WHITE,
        bgColor = Color.BLACK
)