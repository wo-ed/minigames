package com.lmu.pem.minigames.games.camera.racing

import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.viewport.FillViewport
import com.lmu.pem.minigames.*
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.games.GameScreen
import com.lmu.pem.minigames.osmanager.Face
import ktx.box2d.body
import ktx.log.info


class RacingGameScreen : GameScreen(uiStage = halfHeightUiStage()), ContactListener {
    override val assets = listOf(Assets.car, Assets.raceTrack, Assets.raceMusic)
    private val batch: Batch = context.inject()

    private val viewportWidth: Float = DEFAULT_GAME_VIEWPORT_WIDTH / 2
    private val viewportHeight: Float = DEFAULT_GAME_VIEWPORT_HEIGHT / 2
    private val camera = OrthographicCamera()
    private val viewport = FillViewport(viewportWidth, viewportHeight, camera)

    private val world = World(Vector2.Zero, true)
    private val debugRenderer = Box2DDebugRenderer()
    private var isRenderDebugEnabled = false
    private var timeAccumulator = 0f

    var face: Face? = null

    private val map by lazy { assetStorage.get<TiledMap>(Assets.RACE_TRACK) }
    private val mapRenderer by lazy {
        map?.let { OrthogonalTiledMapObjectRenderer(it, MAP_UNIT_SCALE, context.inject()) }
    }

    private val objectScale: Float = MAP_UNIT_SCALE * MAP_UNIT_SCALE
    private lateinit var car: Car
    private lateinit var finish: Body

    private var finishCounter = 0
    var listener: InteractionListener? = null

    private val xControls = when (Gdx.app.type) {
        Application.ApplicationType.Android -> TouchControls(horizontal = true)
        Application.ApplicationType.Desktop -> ArrowKeysControls(horizontal = true)
        else -> throw RuntimeException()
    }

    private val yControls = when (Gdx.app.type) {
        Application.ApplicationType.Android -> EyeControls(::face)
        Application.ApplicationType.Desktop -> ArrowKeysControls(horizontal = false)
        else -> throw RuntimeException()
    }

    init {
        world.setContactListener(this)
    }

    override suspend fun initialize() {
        super.initialize()
        bgColor = Color.valueOf(map!!.properties["backgroundcolor"].toString())
        initializeCar()
        initializeColliders()
        initializeFinish()
        val raceMusic: Music = assetStorage[Assets.RACE_MUSIC]!!
        raceMusic.play()
        raceMusic.isLooping = true
    }

    private fun initializeCar() {
        val props = map!!.layers["markers"].objects["car"].properties
        val x = props["x"] as Float * objectScale
        val y = props["y"] as Float * objectScale
        val angle = (360f - props["rotation"] as Float) * DEG_TO_RAD
        car = Car(world, x, y, angle)
    }

    private fun initializeColliders() {
        val colliders = map!!.layers["colliders"].objects
        MapBodyBuilder.buildBodies(colliders, world, objectScale)
    }

    private fun initializeFinish() {
        val mapObj = map!!.layers["markers"].objects["finish"]
        val shape = MapBodyBuilder.getShape(mapObj, objectScale)!!

        finish = world.body {
            type = BodyDef.BodyType.StaticBody
            fixture(shape) {
                density = 1f
                isSensor = true
            }
        }

        shape.dispose()
    }

    override fun updateGame(delta: Float) {
        super.updateGame(delta)

        val controlState = getControlState()
        car.updateControlState(controlState)

        doPhysicsStep(delta)

        car.synchronizeSprite()
        moveCamera()
    }

    private fun getControlState(): Int {
        val csX = xControls.controlState
        val csY = yControls.controlState

        val xDirection = when {
            csX < 0 -> TDC_LEFT
            csX > 0 -> TDC_RIGHT
            else -> TDC_NONE
        }

        val yDirection = when {
            csY < 0 -> TDC_DOWN
            csY > 0 -> TDC_UP
            else -> TDC_NONE
        }

        return xDirection or yDirection
    }

    private fun doPhysicsStep(deltaTime: Float) {
        // fixed time step
        // max frame time to avoid spiral of death (on slow devices)
        val frameTime = Math.min(deltaTime, 0.25f)
        timeAccumulator += frameTime

        val timeStep = 1 / 60f
        val velocityIterations = 6
        val positionIterations = 2

        while (timeAccumulator >= 1 / 60f) {
            world.step(timeStep, velocityIterations, positionIterations)
            timeAccumulator -= timeStep
        }
    }

    private fun moveCamera() {
        camera.position.x = car.sprite.x + car.sprite.width / 2f
        camera.position.y = car.sprite.y + car.sprite.height / 2f
    }

    override fun drawBackground() {
        drawBackground(viewport)
    }

    override fun drawGame(delta: Float) {
        super.drawGame(delta)

        viewport.apply()

        mapRenderer!!.setView(camera)
        mapRenderer!!.render()

        batch.projectionMatrix = viewport.camera.combined
        batch.begin()
        car.draw(batch)
        batch.end()

        if (isRenderDebugEnabled) {
            debugRenderer.render(world, camera.combined)
        }
    }

    override fun preSolve(contact: Contact, oldManifold: Manifold) {}

    override fun postSolve(contact: Contact, impulse: ContactImpulse) {}

    override fun beginContact(contact: Contact) {
        if (isCarContactingFinish(contact)) {
            if (car.body.linearVelocity.x > 0) {
                finishCounter++
            }

            if (finishCounter > 1) {
                listener?.isGameFinished()
            }

            info { "Begin contact (car, finish), counter: $finishCounter" }
        }
    }

    override fun endContact(contact: Contact) {
        if (isCarContactingFinish(contact)) {
            if (car.body.linearVelocity.x < 0 && finishCounter > 0) {
                finishCounter--
            }

            info { "End contact (car, finish), counter: $finishCounter" }
        }
    }

    private fun isCarContactingFinish(contact: Contact): Boolean {
        val bodyA = contact.fixtureA.body
        val bodyB = contact.fixtureB.body

        return bodyA === car.body && bodyB === finish ||
                bodyA === finish && bodyB === car.body
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport.update(width, height, true)
    }

    override fun dispose() {
        super.dispose()
        debugRenderer.dispose()
        world.dispose()
        mapRenderer?.dispose()
    }

    interface InteractionListener {
        fun isGameFinished()
    }

    companion object {
        private const val MAP_UNIT_SCALE = 0.75f
    }
}