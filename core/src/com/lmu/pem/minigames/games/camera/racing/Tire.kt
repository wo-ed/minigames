package com.lmu.pem.minigames.games.camera.racing

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import ktx.box2d.body
import ktx.math.times
import ktx.math.unaryMinus

const val TDC_NONE = 0x0
const val TDC_LEFT = 0x1
const val TDC_RIGHT = 0x2
const val TDC_UP = 0x4
const val TDC_DOWN = 0x8

class Tire(world: World, carX: Float, carY: Float, carAngle: Float) {
    var maxForwardSpeed: Float = 0f
    var maxBackwardSpeed: Float = 0f
    var maxDriveForce: Float = 0f
    var maxLateralImpulse: Float = 0f

    private val isWakeEnabled = true

    val body = world.body {
        type = BodyDef.BodyType.DynamicBody
        position.set(carX, carY)
        angle = carAngle
        box(width = 0.5f, height = 1.25f) {
            density = 1f
        }
        userData = this
    }

    fun setCharacteristics(maxForwardSpeed: Float, maxBackwardSpeed: Float, maxDriveForce: Float, maxLateralImpulse: Float) {
        this.maxForwardSpeed = maxForwardSpeed
        this.maxBackwardSpeed = maxBackwardSpeed
        this.maxDriveForce = maxDriveForce
        this.maxLateralImpulse = maxLateralImpulse
    }

    fun getLateralVelocity(): Vector2 {
        val currentRightNormal = body.getWorldVector(Vector2(1f, 0f))
        return currentRightNormal * currentRightNormal.dot(body.linearVelocity)
    }

    fun getForwardVelocity(): Vector2 {
        val currentForwardNormal = body.getWorldVector(Vector2(0f, 1f))
        return currentForwardNormal * currentForwardNormal.dot(body.linearVelocity)
    }

    fun updateFriction() {
        var impulse = -getLateralVelocity() * body.mass
        if (impulse.len() > maxLateralImpulse) {
            impulse *= maxLateralImpulse / impulse.len()
        }
        body.applyLinearImpulse(impulse, body.worldCenter, isWakeEnabled)

        body.applyAngularImpulse(0.1f * body.inertia * -body.angularVelocity, isWakeEnabled)

        val currentForwardNormal = getForwardVelocity()
        val currentForwardSpeed = currentForwardNormal.nor()
        val dragForceMagnitude = currentForwardSpeed * -2
        body.applyForce(dragForceMagnitude * currentForwardNormal, body.worldCenter, isWakeEnabled)
    }

    fun updateDrive(controlState: Int) {
        val desiredSpeed = when (controlState and (TDC_UP or TDC_DOWN)) {
            TDC_UP -> maxForwardSpeed
            TDC_DOWN -> maxBackwardSpeed
            else -> return
        }

        //find current speed in forward direction
        val currentForwardNormal = body.getWorldVector(Vector2(0f, 1f))
        val currentSpeed = getForwardVelocity().dot(currentForwardNormal)

        //apply necessary force
        val force = when {
            desiredSpeed > currentSpeed -> maxDriveForce
            desiredSpeed < currentSpeed -> -maxDriveForce
            else -> return
        }

        body.applyForce(currentForwardNormal * force, body.worldCenter, isWakeEnabled)
    }

    fun updateTurn(controlState: Int) {
        val desiredTorque = when (controlState and (TDC_LEFT or TDC_RIGHT)) {
            TDC_LEFT -> 15f
            TDC_RIGHT -> -15f
            else -> 0f
        }
        body.applyTorque(desiredTorque, isWakeEnabled)
    }
}