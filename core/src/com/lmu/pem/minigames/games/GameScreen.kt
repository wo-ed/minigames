package com.lmu.pem.minigames.games

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.utils.viewport.Viewport
import com.lmu.pem.minigames.PADDING
import com.lmu.pem.minigames.ScreenFlowManager
import com.lmu.pem.minigames.assets.onClickWithSound
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.defaultUiStage
import com.lmu.pem.minigames.ui.screens.Clearable
import com.lmu.pem.minigames.ui.screens.StageScreen
import ktx.actors.txt
import ktx.scene2d.table
import ktx.scene2d.textButton

abstract class GameScreen(
        gameStage: Stage? = null,
        override val uiStage: Stage = defaultUiStage(),
        private val isTimeLabelEnabled: Boolean = true,
        var isDrawBordersEnabled: Boolean = false,
        protected var isSkipButtonEnabled: Boolean = true
) : StageScreen(
        gameStage,
        uiStage,
        context.inject()
) {
    private val screenFlowManager: ScreenFlowManager = context.inject()
    private val shapeRenderer: ShapeRenderer = context.inject()

    protected val gameControlsTable = table {}
    private val gameRootTable = table {}
    protected val timeLabel = Label("", context.inject<Skin>())
    protected val titleLabel = Label("", context.inject<Skin>())
    private val pauseDialog = Window("", context.inject<Skin>())

    var title: String
        get() = titleLabel.txt
        set(value) {
            titleLabel.txt = value
        }

    var isPaused = false
        set(value) {
            field = value
            pauseDialog.isVisible = value
            setGameControlsTouchable(!value)
        }

    override suspend fun initialize() {
        super.initialize()
        initBaseLayout()
    }

    protected fun initBaseLayout() {
        gameControlsTable.defaults().pad(PADDING)

        gameRootTable.apply {
            setFillParent(true)
            if (isTimeLabelEnabled) {
                table {
                    it.fill()
                    defaults().pad(PADDING)
                    add(titleLabel)
                    add(timeLabel).expandX().right()
                }
                row()
            }
            add(gameControlsTable).expand()
        }

        pauseDialog.apply {
            isModal = true
            isMovable = false
            isVisible = isPaused

            addActor(table {
                setFillParent(true)
                defaults().pad(PADDING)
                if(isSkipButtonEnabled) {
                    textButton("Skip", "lightLabel") {
                        onClickWithSound { screenFlowManager.skipGame() }
                        color = Color.BLACK
                    }
                    row()
                }
                textButton("Resume") {
                    onClickWithSound { screenFlowManager.toggleGamePaused() }
                }
                row()
                textButton("Quit") {
                    onClickWithSound { screenFlowManager.returnToMenu() }
                }
            })
        }

        val pauseDialogTable = table {
            setFillParent(true)
            defaults().pad(PADDING)
            add(pauseDialog).size(300f, 200f)
        }

        uiStage.addActor(gameRootTable)
        uiStage.addActor(pauseDialogTable)
    }

    fun showTimeLeft(timeLeft: Long) {
        timeLabel.txt = "${timeLeft.div(1000)}"
    }

    private fun setGameControlsTouchable(isTouchable: Boolean) {
        gameRootTable.touchable =
                if (isTouchable) Touchable.enabled
                else Touchable.disabled
    }

    override fun onBackPressed() {
        screenFlowManager.toggleGamePaused()
    }

    protected open fun setLabelStyle(style: Label.LabelStyle) {
        timeLabel.style = style
        titleLabel.style = style
    }

    /** Renders the game AND the control elements on top. */
    override fun render(delta: Float) {
        if (!isPaused) {
            updateGame(delta)
        }

        drawBackground()
        drawGame(delta)
        renderUiStage(delta)
    }

    protected open fun drawBackground() {
        val viewport = gameStage?.viewport

        if (viewport != null) {
            drawBackground(viewport)
        } else {
            clearColor()
        }
    }

    protected fun drawBackground(viewport: Viewport) {
        if (isDrawBordersEnabled) {
            Clearable.clearColor(Color.BLACK)
            drawViewportBackground(viewport)
        } else {
            clearColor()
        }
    }

    protected fun drawViewportBackground(viewport: Viewport) {
        viewport.apply()
        shapeRenderer.apply {
            color = bgColor
            setAutoShapeType(true)
            projectionMatrix = viewport.camera.combined
            begin()
            set(ShapeRenderer.ShapeType.Filled)
            rect(
                    viewport.camera.position.x - viewport.worldWidth / 2f,
                    viewport.camera.position.y - viewport.worldHeight / 2f,
                    viewport.worldWidth,
                    viewport.worldHeight
            )
            end()
        }
    }

    /** Draws the actual game, NOT the control elements.
     * This method IS called when the screen is paused. */
    protected open fun drawGame(delta: Float) {
        gameStage?.viewport?.apply()
        gameStage?.draw()
    }

    /** Updates the actual game, NOT the control elements.
     *  This method is NOT called when the screen is paused. */
    protected open fun updateGame(delta: Float) {
        gameStage?.act(delta)
    }
}