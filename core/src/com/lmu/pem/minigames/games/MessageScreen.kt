package com.lmu.pem.minigames.games

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Align
import com.lmu.pem.minigames.context
import ktx.actors.txt

class MessageScreen : GameScreen(
        isTimeLabelEnabled = false,
        isDrawBordersEnabled = false,
        isSkipButtonEnabled = false
) {
    override val assets: Iterable<AssetDescriptor<*>>? = null

    private val messageLabel = Label("", context.inject<Skin>())

    var message: String? = null
        set(value) {
            field = value
            messageLabel.txt = value ?: ""
        }

    override suspend fun initialize() {
        super.initialize()
        initLayout()
    }

    private fun initLayout() {
        messageLabel.setAlignment(Align.center, Align.center)
        gameControlsTable.add(messageLabel)
    }
}