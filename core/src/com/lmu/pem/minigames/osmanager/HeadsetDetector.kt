package com.lmu.pem.minigames.osmanager

interface HeadsetDetector {
    fun isWiredHeadsetOn(): Boolean
}