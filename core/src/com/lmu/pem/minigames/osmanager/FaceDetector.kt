package com.lmu.pem.minigames.osmanager

interface FaceDetector {
    fun setUpCamera(front: Boolean)
    fun startCamera()
    fun stopCamera()
    fun pauseCamera()
    fun resumeCamera()
    fun showCamera()
    fun hideCamera()
    fun startFaceDetection(callback: (Iterable<Face>) -> Unit)
    fun stopFaceDetection()
}