package com.lmu.pem.minigames.osmanager

/** Manages OS-specific device features */
interface OsManager {
    val faceDetector: FaceDetector?
    val headsetDetector: HeadsetDetector?
}