package com.lmu.pem.minigames.ui.screens.menu

import com.lmu.pem.minigames.PADDING
import com.lmu.pem.minigames.assets.onClickWithSound
import ktx.scene2d.table
import ktx.scene2d.textButton

class MainMenuScreen : MenuScreen() {

    override suspend fun initialize() {
        super.initialize()
        initLayout()
    }

    private fun initLayout() {
        val table = table {
            setFillParent(true)
            defaults().pad(PADDING)
            textButton("Tutorial") {
                onClickWithSound { screenFlowManager.startStoryMode() }
            }
            row()
            textButton("Survival") {
                onClickWithSound { screenFlowManager.startSurvivalMode() }
            }
            row()
            textButton("Choose") {
                onClickWithSound { screenFlowManager.showChooseGameScreen() }
            }
        }

        uiStage.addActor(table)
    }

    override fun onBackPressed() {
        screenFlowManager.exit()
    }
}