package com.lmu.pem.minigames.ui.screens

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.scenes.scene2d.Stage

/** A simple screen that uses two stages for rendering and user input.
 * Two stages are used because every stage uses a different viewport.
 *
 * @param gameStage     Used for the game elements
 * @param uiStage       Used for the control elements
 * @param inputMultiplexer     Handles the user input */
abstract class StageScreen(
        protected open val gameStage: Stage? = null,
        protected open val uiStage: Stage? = null,
        private val inputMultiplexer: InputMultiplexer? = null
) : BaseScreen() {

    override fun render(delta: Float) {
        super.render(delta)
        renderGameStage(delta)
        renderUiStage(delta)
    }

    fun renderGameStage(delta: Float) {
        gameStage?.viewport?.apply()
        gameStage?.act(delta)
        gameStage?.draw()
    }

    fun renderUiStage(delta: Float) {
        uiStage?.viewport?.apply()
        uiStage?.act(delta)
        uiStage?.draw()
    }

    override fun show() {
        super.show()
        uiStage?.let { inputMultiplexer?.addProcessor(it) }
        gameStage?.let { inputMultiplexer?.addProcessor(it) }
    }

    override fun hide() {
        super.hide()
        uiStage?.let { inputMultiplexer?.removeProcessor(it) }
        gameStage?.let { inputMultiplexer?.removeProcessor(it) }
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        gameStage?.viewport?.update(width, height, true)
        uiStage?.viewport?.update(width, height, true)
    }

    override fun dispose() {
        super.dispose()
        uiStage?.let { inputMultiplexer?.removeProcessor(it) }
        gameStage?.let { inputMultiplexer?.removeProcessor(it) }
        uiStage?.dispose()
        gameStage?.dispose()
    }
}