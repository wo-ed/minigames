package com.lmu.pem.minigames.ui.screens.menu

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.FillViewport
import com.lmu.pem.minigames.ScreenFlowManager
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.defaultUiStage
import com.lmu.pem.minigames.ui.screens.StageScreen

abstract class MenuScreen : StageScreen(inputMultiplexer = context.inject()) {
    override val uiStage = defaultUiStage()
    protected val batch: Batch = context.inject()
    private val textureViewport = FillViewport(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())

    protected val screenFlowManager: ScreenFlowManager = context.inject()

    override val assets: Iterable<AssetDescriptor<*>>? = null

    private val texture by lazy {
        assetStorage.get<Texture>(Assets.BACKGROUND)!!.apply {
            setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        }
    }

    override suspend fun initialize() {
        super.initialize()
        //bgColor = Color.BLACK
    }

    override fun render(delta: Float) {
        clearColor()
        renderBackground()
        renderUiStage(delta)
    }

    private fun renderBackground() {
        val width = texture.width / texture.height * textureViewport.worldHeight
        val height = textureViewport.worldHeight

        textureViewport.apply()
        batch.projectionMatrix = textureViewport.camera.combined
        batch.begin()
        batch.draw(
                texture,
                textureViewport.camera.position.x - width / 2f,
                textureViewport.camera.position.y - height / 2f,
                width,
                height
        )
        batch.end()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        textureViewport.update(width, height, true)
    }

    /*
    override suspend fun initialize() {
        super.initialize()

        val actor = TextureActor(texture)
        val table = table {
            setFillParent(true)
            add(actor).width(Value.percentWidth(1f, this@table)).height(Value.percentHeight(1f, this@table))
        }
        uiStage.addActor(table)
    }*/
}