package com.lmu.pem.minigames.ui.screens.menu

import com.lmu.pem.minigames.PADDING
import com.lmu.pem.minigames.assets.onClickWithSound
import com.lmu.pem.minigames.games.games
import ktx.scene2d.KTableWidget
import ktx.scene2d.scrollPane
import ktx.scene2d.table
import ktx.scene2d.textButton

class ChooseScreen : MenuScreen() {

    override suspend fun initialize() {
        super.initialize()
        initLayout()
    }

    private fun initLayout() {
        val table = table {
            setFillParent(true)
            scrollPane {
                table {
                    setFillParent(true)
                    defaults().pad(PADDING)
                    createGameLabels()
                }
            }
        }

        uiStage.addActor(table)
    }

    private fun KTableWidget.createGameLabels() {
        games.values.forEach { gameCreator ->
            textButton(gameCreator.title) {
                onClickWithSound {
                    screenFlowManager.startSingleGameMode(gameCreator)
                }
            }
            row()
        }
    }

    override fun onBackPressed() {
        screenFlowManager.returnToMenu()
    }
}