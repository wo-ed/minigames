package com.lmu.pem.minigames.ui.screens

import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.assets.isLoaded
import com.lmu.pem.minigames.assets.load
import com.lmu.pem.minigames.assets.unload
import com.lmu.pem.minigames.context
import ktx.async.assets.AssetStorage

abstract class BaseScreen : ScreenAdapter(), Clearable {
    protected val assetStorage: AssetStorage = context.inject()

    override var bgColor = Color.WHITE!!

    abstract val assets: Iterable<AssetDescriptor<*>>?

    open suspend fun initialize() {}

    override fun render(delta: Float) {
        clearColor()
    }

    abstract fun onBackPressed()

    suspend fun loadAssets() = assets?.load(assetStorage)

    fun unloadAssets() = assets?.unload(assetStorage)

    fun areAssetsLoaded() = assets?.isLoaded(assetStorage) ?: true

    override fun dispose() {
        super.dispose()
        unloadAssets()
    }
}