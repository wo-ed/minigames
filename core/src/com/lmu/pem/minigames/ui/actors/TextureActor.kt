package com.lmu.pem.minigames.ui.actors

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Batch

open class TextureActor() : AnimatedActor<Texture>() {
    constructor(texture: Texture?) : this() {
        this.texture = texture
        synchronizeSize()
    }

    constructor(animation: Animation<Texture>?) : this() {
        this.animation = animation
        synchronizeSize()
    }

    var texture: Texture?
        get() = image
        set(value) {
            image = value
        }

    fun synchronizeSize() {
        width = texture?.width?.toFloat() ?: 0f
        height = texture?.height?.toFloat() ?: 0f
        originX = width / 2f
        originY = height / 2f
    }

    override fun draw(batch: Batch, parentAlpha: Float) {
        texture?.let {
            batch.draw(it, x, y, originX, originY, width, height, scaleX, scaleY, rotation,
                    0, 0, it.width, it.height, flipX, flipY)
        }
    }
}