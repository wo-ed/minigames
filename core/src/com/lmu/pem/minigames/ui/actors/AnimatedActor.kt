package com.lmu.pem.minigames.ui.actors

import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.scenes.scene2d.Actor

abstract class AnimatedActor<I> : Actor() {
    protected var image: I? = null

    var flipX = false
    var flipY = false

    var counter = 0f

    var animation: Animation<I>? = null
        set(value) {
            field = value
            resetCounter()
            getKeyFrame()?.let { image = it }
        }

    fun resetCounter() {
        counter = 0f
    }

    protected fun getKeyFrame() = animation?.getKeyFrame(counter)

    override fun act(delta: Float) {
        super.act(delta)
        counter += delta
        getKeyFrame()?.let { image = it }
    }
}