package com.lmu.pem.minigames.modes

import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.games.GameCreator
import com.lmu.pem.minigames.games.Minigame
import com.lmu.pem.minigames.games.games
import ktx.async.ktxAsync

open class SurvivalMode(
        gameIterator: Iterator<GameCreator> = games.values.shuffled().iterator()
) : GameMode(gameIterator) {

    private val noHpLeftSound: Sound = assetStorage[Assets.GAME_OVER_3]!!
    override val noGamesLeftSound: Sound = assetStorage[Assets.WIN_2]!!

    override var activeGame: Minigame<*>?
        get() = super.activeGame
        set(value) {
            super.activeGame = value
            timeLeft = value?.time
        }

    private var timeLeft: Long? = null
    private var hp: Int = INITIAL_HP

    private val isTimeUp: Boolean
        get() {
            val timeLeft = this.timeLeft
            return timeLeft != null && timeLeft <= 0
        }

    override fun updateMode(delta: Float) {
        timeLeft = timeLeft?.minus(delta.times(1000).toLong())
        activeGame?.screen?.showTimeLeft(timeLeft ?: 0L)
    }

    private val areHpLeft get() = hp > 0
    override val isGameFailed get() = super.isGameFailed || isTimeUp
    override val winningMessage get() = hpString
    override val losingMessage get() = hpString
    override val startingMessage get() = hpString
    private val hpString get() = "X".repeat(hp)

    override fun loseGame() {
        hp -= 1
        if (areHpLeft) {
            super.loseGame()
        } else {
            noHpLeft()
        }
    }

    private fun noHpLeft() {
        activeGame?.dispose()
        activeGame = null

        ktxAsync {
            showMessageScreen("Game over", Color.RED)
            noHpLeftSound.play()
        }
    }

    override fun dispose() {
        super.dispose()
        noHpLeftSound.stop()
    }

    companion object {
        const val INITIAL_HP = 4
    }
}