package com.lmu.pem.minigames.modes

import com.badlogic.gdx.graphics.Color
import com.lmu.pem.minigames.games.games

class StoryMode : GameMode(games.values.iterator()) {

    override val losingMessage: String
        get() = activeGame?.hint ?: super.losingMessage

    override val startingMessage: String
        get() = activeGame?.hint ?: super.startingMessage

    override val winningMessage: String
        get() = nextGame?.hint ?: super.winningMessage

    override val skippingMessage: String
        get() = nextGame?.hint ?: super.skippingMessage

    override fun loseGame() {
        activeGame?.let {
            it.dispose()
            activeGame = games[it::class]!!.create()
        }
        startGameAfterMessage(losingMessage, Color.RED)
        failSound.play()
    }

    override fun updateMode(delta: Float) {}
}