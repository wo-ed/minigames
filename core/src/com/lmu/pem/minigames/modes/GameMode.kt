package com.lmu.pem.minigames.modes

import com.badlogic.gdx.Screen
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.utils.Disposable
import com.lmu.pem.minigames.MyGdxGame
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.context
import com.lmu.pem.minigames.games.GameCreator
import com.lmu.pem.minigames.games.MessageScreen
import com.lmu.pem.minigames.games.Minigame
import ktx.async.assets.AssetStorage
import ktx.async.ktxAsync

abstract class GameMode(
        private val gameIterator: Iterator<GameCreator>
) : Disposable {

    private val myGdxGame: MyGdxGame = context.inject()
    protected val assetStorage: AssetStorage = context.inject()

    protected open var activeGame: Minigame<*>? = null
    protected var nextGame: Minigame<*>? = null

    private var messageScreen: MessageScreen? = null
    private var taskTimeLeft = 0L
    private var task: (() -> Boolean)? = null

    var isPaused: Boolean = false
        set(value) {
            field = value
            messageScreen?.isPaused = value
            if (value) {
                activeGame?.onPauseGame()
            } else {
                activeGame?.onResumeGame()
            }
        }

    protected open val isGameFinished get() = activeGame?.isFinished == true
    protected open val isGameFailed get() = activeGame?.isFailed == true
    protected val isMessageScreenShown
        get() = messageScreen !== null
                && messageScreen === myGdxGame.screen
    protected open val winningMessage = "Great!"
    protected open val losingMessage = "Try it again"
    protected open val startingMessage = "Start"
    protected open val skippingMessage = "Skipped"

    protected open val noGamesLeftSound: Sound = assetStorage[Assets.WIN]!!
    protected open val successSound: Sound = assetStorage[Assets.SUCCESS_5]!!
    protected open val failSound: Sound = assetStorage[Assets.FAIL]!!

    fun start() {
        activeGame = createNextGame()
        nextGame = createNextGame()
        startGameAfterMessage(startingMessage)
    }

    protected fun startGame(): Boolean {
        val activeGame = this.activeGame

        return when {
            activeGame == null -> false
            activeGame.isInitialized -> {
                showScreen(activeGame.screen)
                activeGame.onStartGame()
                true
            }
            else -> false
        }
    }

    protected fun startGameAfterMessage(message: String, bgColor: Color? = null) {
        if (this.task != null) {
            return
        }

        scheduleTask { startGame() }

        ktxAsync {
            showMessageScreen(message, bgColor)
            activeGame?.preload()
        }
    }

    protected open fun winGame() {
        playNextGameAfterMessage(winningMessage, Color.GREEN)
        successSound.play()
    }

    protected open fun loseGame() {
        playNextGameAfterMessage(losingMessage, Color.RED)
        failSound.play()
    }

    open fun skipGame() {
        if (this.task != null || activeGame == null) {
            return
        }

        playNextGameAfterMessage(skippingMessage)
        isPaused = false
    }

    protected fun playNextGameAfterMessage(message: String, bgColor: Color? = null) {
        if (this.task != null) {
            return
        }

        activeGame?.dispose()
        activeGame = null

        scheduleTask { playNextGame() }

        ktxAsync {
            showMessageScreen(message, bgColor)
            nextGame?.preload()
        }
    }

    private fun playNextGame(): Boolean {
        val nextGame = this.nextGame

        return if (nextGame == null) {
            noGamesleft()
            true
        } else if (!nextGame.isInitialized) {
            false
        } else {
            this.activeGame = nextGame
            this.nextGame = createNextGame()
            startGame()
            true
        }
    }

    protected open fun createNextGame(): Minigame<*>? {
        return if (gameIterator.hasNext()) {
            val gameCreator = gameIterator.next()
            gameCreator.create()
        } else null
    }

    protected open fun noGamesleft() {
        activeGame?.dispose()
        activeGame = null

        ktxAsync {
            showMessageScreen("Mode complete", Color.BLUE)
            noGamesLeftSound.play()
        }
    }

    protected fun scheduleTask(delayMillis: Long = DEFAULT_TASK_TIME, task: () -> Boolean) {
        if (this.task != null) {
            return
        }

        taskTimeLeft = delayMillis
        this.task = task
    }

    private fun showScreen(screen: Screen) {
        myGdxGame.disposeScreen()
        myGdxGame.screen = screen
    }

    protected open suspend fun showMessageScreen(message: String, bgColor: Color? = null) {
        // Show screen before it is loaded so that the game is paused
        val screen = MessageScreen()
        this.messageScreen = screen
        screen.message = message
        bgColor?.let { screen.bgColor = it }
        showScreen(screen)

        // Load the screen asynchronously
        screen.loadAssets()
        screen.initialize()
    }

    fun pause() {
        isPaused = true
    }

    fun resume() {
        isPaused = false
    }

    fun update(delta: Float) {
        if (isPaused) {
            return
        }

        val task = this.task
        if (task != null) {
            taskTimeLeft -= delta.times(1000).toLong()
            if (taskTimeLeft <= 0 && task()) {
                this.task = null
            }

            return
        }

        if (isMessageScreenShown) {
            return
        }

        if (isGameFinished) {
            winGame()
            return
        }

        if (isGameFailed) {
            loseGame()
            return
        }

        updateMode(delta)
    }

    protected abstract fun updateMode(delta: Float)

    override fun dispose() {
        noGamesLeftSound.stop()
        successSound.stop()
        failSound.stop()

        activeGame?.dispose()
        nextGame?.dispose()
        messageScreen?.dispose()
    }

    companion object {
        const val DEFAULT_TASK_TIME = 3000L
    }
}