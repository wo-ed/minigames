package com.lmu.pem.minigames.modes

import com.lmu.pem.minigames.InfiniteSingleItemIterator
import com.lmu.pem.minigames.games.GameCreator

class SingleGameMode(gameCreator: GameCreator)
    : SurvivalMode(InfiniteSingleItemIterator(gameCreator)) {
}