package com.lmu.pem.minigames.assets

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.lmu.pem.minigames.assets.Assets.background
import com.lmu.pem.minigames.assets.Assets.click
import com.lmu.pem.minigames.assets.Assets.comicFont
import com.lmu.pem.minigames.assets.Assets.comicSkin
import com.lmu.pem.minigames.assets.Assets.fail
import com.lmu.pem.minigames.assets.Assets.gameOver3
import com.lmu.pem.minigames.assets.Assets.success5
import com.lmu.pem.minigames.assets.Assets.win
import com.lmu.pem.minigames.assets.Assets.win2

object Assets {

    // Folders
    const val MENU = "menu"
    const val SKINS = "$MENU/skins"
    const val FONTS = "$MENU/fonts"
    const val TEXTURES = "textures"
    const val SOUNDS = "sounds"
    const val MAPS = "maps"

    // Menus
    const val CLICK = "$SOUNDS/171697__nenadsimic__menu-selection-click.ogg"
    const val BACKGROUND = "$TEXTURES/background.png"
    const val NOTIFICATION = "$SOUNDS/242502__gabrielaraujo__pop-up-notification.ogg"
    const val COMIC_SKIN = "$SKINS/comic/comic-ui.json"
    const val COMIC_FONT = "$FONTS/komika-display.ttf"
    val background = AssetDescriptor(BACKGROUND, Texture::class.java)
    val click = AssetDescriptor(CLICK, Sound::class.java)
    val notification = AssetDescriptor(NOTIFICATION, Sound::class.java)
    val comicSkin = AssetDescriptor(COMIC_SKIN, Skin::class.java)
    val comicFont = AssetDescriptor(
            COMIC_FONT,
            BitmapFont::class.java,
            FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
                fontFileName = COMIC_FONT
                fontParameters.size = 32
                fontParameters.color = Color.WHITE
            }
    )

    // Face detection
    const val STARE = "$TEXTURES/stare.png"
    const val TUNNEL = "$TEXTURES/tunnel.png"
    val stare = AssetDescriptor(STARE, Texture::class.java)
    val tunnel = AssetDescriptor(TUNNEL, Texture::class.java)

    // Sail
    const val BOAT = "$TEXTURES/boat.png"
    const val GOAL = "$TEXTURES/goal.png"
    const val HARBOUR_WALL = "$TEXTURES/harbour_wall.png"
    const val ISLAND = "$TEXTURES/island.png"
    const val ISLAND_PIRATE = "$TEXTURES/island_pirate.png"
    const val STONES = "$TEXTURES/stones.png"
    const val LIGHTHOUSE = "$TEXTURES/lighthouse.png"
    const val SEA_TRACK = "$SOUNDS/ocean_sound.ogg"

    val boat = AssetDescriptor(BOAT, Texture::class.java)
    val goal = AssetDescriptor(GOAL, Texture::class.java)
    val harbourWall = AssetDescriptor(HARBOUR_WALL, Texture::class.java)
    val island = AssetDescriptor(ISLAND, Texture::class.java)
    val pirateIsland = AssetDescriptor(ISLAND_PIRATE, Texture::class.java)
    val stones = AssetDescriptor(STONES, Texture::class.java)
    val lighthouse = AssetDescriptor(LIGHTHOUSE, Texture::class.java)
    val oceanSailTrack = AssetDescriptor(SEA_TRACK, Music::class.java)


    //FishAttack
    const val PUFFERFISH = "$TEXTURES/pufferfish.png"
    const val SWORDFISH = "$TEXTURES/swordfish.png"
    const val SEABG = "$TEXTURES/seabackground.png"
    val pufferfish = AssetDescriptor(PUFFERFISH, Texture::class.java)
    val swordfish = AssetDescriptor(SWORDFISH, Texture::class.java)
    val seabg = AssetDescriptor(SEABG, Texture::class.java)

    // Balloon
    const val GREEN_BALLOON = "$TEXTURES/green_balloon.png"
    const val RED_BALLOON = "$TEXTURES/red_balloon.png"
    const val CROSSHAIR = "$TEXTURES/crosshair.png"
    val green_balloon = AssetDescriptor(GREEN_BALLOON, Texture::class.java)
    val red_balloon = AssetDescriptor(RED_BALLOON, Texture::class.java)
    val cross = AssetDescriptor(CROSSHAIR, Texture::class.java)

    //Jump
    const val MARIO = "$TEXTURES/mario.png"
    val mario = AssetDescriptor(MARIO, Texture::class.java)
    const val GOOMBA = "$TEXTURES/goomba.png"
    val goomba = AssetDescriptor(GOOMBA, Texture::class.java)

    const val SHELL = "$TEXTURES/spiny_shell.png"
    val shell = AssetDescriptor(SHELL, Texture::class.java)

    const val BOY = "$TEXTURES/boy.png"
    val boy = AssetDescriptor(BOY, Texture::class.java)

    //Hole
    const val BBALL = "$TEXTURES/basketball.png"
    val basketball_texture = AssetDescriptor(BBALL, Texture::class.java)

    const val RED_BALL = "$TEXTURES/red_ball.png"
    val red_ball = AssetDescriptor(RED_BALL, Texture::class.java)

    const val BLUE_BALL = "$TEXTURES/blue_ball.gif"
    val blue_ball = AssetDescriptor(BLUE_BALL, Texture::class.java)

    const val GREEN_BALL = "$TEXTURES/green_ball.png"
    val green_ball = AssetDescriptor(GREEN_BALL, Texture::class.java)

    const val YELLOW_BALL = "$TEXTURES/yellow_ball.png"
    val yellow_ball = AssetDescriptor(YELLOW_BALL, Texture::class.java)

    const val RED_CIRCLE = "$TEXTURES/red_circle.png"
    val red_circle = AssetDescriptor(RED_CIRCLE, Texture::class.java)

    const val RED_HOUSE = "$TEXTURES/red_house.png"
    val red_house = AssetDescriptor(RED_HOUSE, Texture::class.java)

    const val GREEN_HOUSE = "$TEXTURES/green_house.png"
    val green_house = AssetDescriptor(GREEN_HOUSE, Texture::class.java)

    const val BLUE_HOUSE = "$TEXTURES/blue_house.png"
    val blue_house = AssetDescriptor(BLUE_HOUSE, Texture::class.java)

    const val YELLOW_HOUSE = "$TEXTURES/yellow_house.png"
    val yellow_house = AssetDescriptor(YELLOW_HOUSE, Texture::class.java)

    //Rock
    const val DISCO_BALL = "$TEXTURES/disco_ball.png"
    val disco_ball = AssetDescriptor(DISCO_BALL, Texture::class.java)

    const val MUSIC = "$TEXTURES/music.png"
    val music = AssetDescriptor(MUSIC, Texture::class.java)

    const val ROCK_MUSIC = "$SOUNDS/guitar.wav"
    val rockMusic = AssetDescriptor(ROCK_MUSIC, Music::class.java)

    val rocker: List<AssetDescriptor<Texture>> = (1 .. 2).map { AssetDescriptor("$TEXTURES/rocker$it.gif", Texture::class.java) }

    // Game modes
    const val WIN = "$SOUNDS/258142__tuudurt__level-win.ogg"
    const val WIN_2 = "$SOUNDS/275104__tuudurt__piglevelwin2.ogg"
    const val WIN_3 = "$SOUNDS/342218__littlerainyseasons__good-end.ogg"
    const val GAME_OVER = "$SOUNDS/133283__fins__game-over.ogg"
    const val GAME_OVER_2 = "$SOUNDS/135831__davidbain__end-game-fail.ogg"
    const val GAME_OVER_3 = "$SOUNDS/371205__mouse85224__level-failed.ogg"
    const val SUCCESS = "$SOUNDS/171670__fins__success-2.ogg"
    const val SUCCESS_2 = "$SOUNDS/171671__fins__success-1.ogg"
    const val SUCCESS_3 = "$SOUNDS/242501__gabrielaraujo__powerup-success.ogg"
    const val SUCCESS_4 = "$SOUNDS/325805__wagna__collect.ogg"
    const val SUCCESS_5 = "$SOUNDS/362445__tuudurt__positive-response.ogg"
    const val FAIL = "$SOUNDS/242503__gabrielaraujo__failure-wrong-action.ogg"
    val win = AssetDescriptor(WIN, Sound::class.java)
    val win2 = AssetDescriptor(WIN_2, Sound::class.java)
    val win3 = AssetDescriptor(WIN_3, Sound::class.java)
    val gameOver = AssetDescriptor(GAME_OVER, Sound::class.java)
    val gameOver2 = AssetDescriptor(GAME_OVER_2, Sound::class.java)
    val gameOver3 = AssetDescriptor(GAME_OVER_3, Sound::class.java)
    val success = AssetDescriptor(SUCCESS, Sound::class.java)
    val success2 = AssetDescriptor(SUCCESS_2, Sound::class.java)
    val success3 = AssetDescriptor(SUCCESS_3, Sound::class.java)
    val success4 = AssetDescriptor(SUCCESS_4, Sound::class.java)
    val success5 = AssetDescriptor(SUCCESS_5, Sound::class.java)
    val fail = AssetDescriptor(FAIL, Sound::class.java)

    // Racing
    const val RACE_TRACK = "$MAPS/race_track.tmx"
    const val CAR = "$TEXTURES/car.png"
    const val ENGINE = "$SOUNDS/421001__eponn__engine.ogg"
    const val RACE_MUSIC = "$SOUNDS/237089__foolboymedia__race-track.ogg"
    val raceTrack = AssetDescriptor(RACE_TRACK, TiledMap::class.java)
    val car = AssetDescriptor(CAR, Texture::class.java)
    val engine = AssetDescriptor(ENGINE, Sound::class.java)
    val raceMusic = AssetDescriptor(RACE_MUSIC, Music::class.java)

    //Balloon
    const val POP = "$SOUNDS/baloon_pop.ogg"
    val balloonPop = AssetDescriptor(POP, Sound::class.java)

    //Jump
    const val JUMP = "$SOUNDS/jump.wav"
    val jumpSound = AssetDescriptor(JUMP, Sound::class.java)

    const val ARGH = "$SOUNDS/argh.wav"
    val arghSound = AssetDescriptor(ARGH, Sound::class.java)



}

val globalAssets: Iterable<AssetDescriptor<*>> = listOf(background, comicSkin, comicFont, click, gameOver3, success5, fail, win, win2)