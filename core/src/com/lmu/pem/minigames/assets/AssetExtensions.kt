package com.lmu.pem.minigames.assets

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.lmu.pem.minigames.context
import ktx.actors.onClick
import ktx.async.assets.AssetStorage

suspend fun AssetStorage.load(assets: Iterable<AssetDescriptor<*>>) = assets.distinct().forEach { load(it) }
fun AssetStorage.unload(assets: Iterable<AssetDescriptor<*>>) = assets.distinct().forEach { unload(it.fileName) }
fun AssetStorage.isLoaded(assets: Iterable<AssetDescriptor<*>>) = assets.distinct().all { isLoaded(it.fileName) }
inline fun <reified T : Any> AssetStorage.get(assets: Iterable<AssetDescriptor<T>>) = assets.distinct().map { get<T>(it.fileName) }

suspend fun Iterable<AssetDescriptor<*>>.load(assetStorage: AssetStorage) = assetStorage.load(this)
fun Iterable<AssetDescriptor<*>>.unload(assetStorage: AssetStorage) = assetStorage.unload(this)
fun Iterable<AssetDescriptor<*>>.isLoaded(assetStorage: AssetStorage) = assetStorage.isLoaded(this)
inline fun <reified T : Any> Iterable<AssetDescriptor<T>>.getAssets(assetStorage: AssetStorage) = assetStorage.get(this)

inline fun Actor.onClickWithSound(crossinline listener: () -> Unit): ClickListener {
    return this.onClick {
        val sound: Sound? = context.inject<AssetStorage>()[Assets.CLICK]
        sound?.play()
        listener()
    }
}