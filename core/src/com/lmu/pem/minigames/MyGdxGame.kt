package com.lmu.pem.minigames

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.lmu.pem.minigames.assets.Assets
import com.lmu.pem.minigames.assets.globalAssets
import com.lmu.pem.minigames.assets.load
import com.lmu.pem.minigames.modes.GameMode
import com.lmu.pem.minigames.osmanager.OsManager
import com.lmu.pem.minigames.ui.screens.Clearable
import ktx.async.assets.AssetStorage
import ktx.async.enableKtxCoroutines
import ktx.async.ktxAsync
import ktx.scene2d.Scene2DSkin

/** The entry point of the game */
class MyGdxGame(val osManager: OsManager) : Game(), Clearable {
    private val assetStorage by lazy { context.inject<AssetStorage>() }
    private val screenFlowManager by lazy { context.inject<ScreenFlowManager>() }
    private val batch by lazy { context.inject<Batch>() }

    override var bgColor = Color.WHITE!!
    var gameMode: GameMode? = null

    override fun create() {
        enableKtxCoroutines(asynchronousExecutorConcurrencyLevel = 1)
        registerProviders()
        configureInputMultiplexer()
        ktxAsync {
            initialize()
        }
    }

    private fun configureInputMultiplexer() {
        val im: InputMultiplexer = context.inject()
        Gdx.input.inputProcessor = im
        Gdx.input.isCatchBackKey = true
        im.addProcessor(BackKeyInputProcessor(this))
    }

    override fun render() {
        clearColor()
        batch.color = Color.WHITE
        gameMode?.update(Gdx.graphics.deltaTime)
        super.render()
    }

    private suspend fun initialize() {
        assetStorage.load(globalAssets)
        initDefaultSkin()
        registerGlobalAssets()
        screenFlowManager.returnToMenu()
    }

    private fun initDefaultSkin() {
        Scene2DSkin.defaultSkin = assetStorage[Assets.COMIC_SKIN]!!
        Scene2DSkin.defaultSkin.apply {
            val comicFont: BitmapFont = assetStorage[Assets.COMIC_FONT]!!

            val defaultLabelStyle = get(Label.LabelStyle::class.java)
            defaultLabelStyle.font = comicFont

            val lightLabelStyle = Label.LabelStyle(comicFont, Color.WHITE)
            add("light", lightLabelStyle)

            val defaultTextButtonStyle = get(TextButton.TextButtonStyle::class.java)
            val lightLabelTextButtonStyle = TextButton.TextButtonStyle(defaultTextButtonStyle)
            lightLabelTextButtonStyle.fontColor = Color.WHITE
            add("lightLabel", lightLabelTextButtonStyle)
        }
    }

    fun reset() {
        gameMode?.dispose()
        gameMode = null
        disposeScreen()
    }

    fun disposeScreen() {
        screen?.dispose()
        screen = null
    }

    override fun dispose() {
        super.dispose()
        gameMode?.dispose()
        disposeScreen()
        context.dispose()
    }
}