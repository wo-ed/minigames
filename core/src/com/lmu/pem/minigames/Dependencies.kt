package com.lmu.pem.minigames

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import ktx.async.assets.AssetStorage
import ktx.inject.Context
import ktx.scene2d.Scene2DSkin

val context by lazy {
    Context()
}

fun registerProviders() = context.register {
    bindSingleton<Batch>(SpriteBatch())
    bindSingleton(ShapeRenderer())
    bindSingleton(InputMultiplexer())
    bindSingleton(Gdx.app.applicationListener as MyGdxGame)
    bindSingleton(ScreenFlowManager())
    bindSingleton(inject<MyGdxGame>().osManager)
    bindSingleton(AssetStorage().apply {
        val resolver = InternalFileHandleResolver()
        setLoader(FreeTypeFontGeneratorLoader(resolver))
        setLoader(BitmapFont::class.java, FreetypeFontLoader(resolver), ".ttf")
        setLoader(TiledMap::class.java, TmxMapLoader(resolver), ".tmx")
    })
}

fun registerGlobalAssets() = context.register {
    bindSingleton(Scene2DSkin.defaultSkin)
}