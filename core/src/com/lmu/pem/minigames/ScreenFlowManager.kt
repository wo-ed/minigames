package com.lmu.pem.minigames

import com.badlogic.gdx.Gdx
import com.lmu.pem.minigames.games.GameCreator
import com.lmu.pem.minigames.modes.GameMode
import com.lmu.pem.minigames.modes.SingleGameMode
import com.lmu.pem.minigames.modes.StoryMode
import com.lmu.pem.minigames.modes.SurvivalMode
import com.lmu.pem.minigames.ui.screens.menu.ChooseScreen
import com.lmu.pem.minigames.ui.screens.menu.MainMenuScreen
import ktx.async.ktxAsync

/** Handles the screens' interactions */
class ScreenFlowManager {
    private val myGdxGame: MyGdxGame = context.inject()

    fun startSurvivalMode() = startGameMode(SurvivalMode())

    fun startStoryMode() = startGameMode(StoryMode())

    fun startSingleGameMode(gameCreator: GameCreator) =
            startGameMode(SingleGameMode(gameCreator))

    private fun startGameMode(gameMode: GameMode) {
        myGdxGame.reset()
        myGdxGame.gameMode = gameMode.apply {
            start()
        }
    }

    fun showChooseGameScreen() {
        myGdxGame.reset()
        myGdxGame.screen = ChooseScreen().apply {
            ktxAsync {
                loadAssets()
                initialize()
            }
        }
    }

    fun showSettings() {
        // TODO
    }

    fun returnToMenu() {
        myGdxGame.reset()
        myGdxGame.screen = MainMenuScreen().apply {
            ktxAsync {
                loadAssets()
                initialize()
            }
        }
    }

    fun exit() = Gdx.app.exit()

    fun toggleGamePaused() {
        myGdxGame.gameMode?.let {
            it.isPaused = !it.isPaused
        }
    }

    fun skipGame() {
        myGdxGame.gameMode?.skipGame()
    }
}